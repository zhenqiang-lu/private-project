define(function (require, exports, module) {
    //获取视频编码
    exports.getVencParam = function (data) {
      return axios({
        url: "/ITS/MediaMng/GetVencParam",
        method: 'post',
        data: data
      })
    }
  
    //设置视频编码
    exports.setVencParam = function (data) {
      return axios({
        url: "/ITS/MediaMng/SetVencParam",
        method: 'post',
        data: data
      })
    }

    //获取音频编码
    exports.getAencParam = function (data) {
      return axios({
        url: "/ITS/MediaMng/GetAencParam",
        method: 'post',
        data: data
      })
    }
  
    //设置音频编码
    exports.setAencParam = function (data) {
      return axios({
        url: "/ITS/MediaMng/SetAencParam",
        method: 'post',
        data: data
      })
    }

    //设置音频编码
    exports.getOSDConfig = function (data) {
      return axios({
        url: "/ITS/MediaMng/GetOSDConfig",
        method: 'post',
        data: data
      })
    }

    //设置音频编码
    exports.setOSDConfig = function (data) {
      return axios({
        url: "/ITS/MediaMng/SetOSDConfig",
        method: 'post',
        data: data
      })
    }
  
  })
  