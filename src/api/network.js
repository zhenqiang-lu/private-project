define(function (require, exports, module) {
  //获取网络信息
  exports.getNetwork = function (data) {
    return axios({
      url: "/ITS/Network/GetNetwork",
      method: 'post',
      data: data
    })
  }

  //设置网络信息
  exports.setNetwork = function (data) {
    return axios({
      url: "/ITS/Network/SetNetwork",
      method: 'post',
      data: data
    })
  }

  //获取端口信息
  exports.getSysPort = function (data) {
    return axios({
      url: "/ITS/Network/GetSysPort",
      method: 'post',
      data: data
    })
  }

  //设置端口信息
  exports.setSysPort = function (data) {
    return axios({
      url: "/ITS/Network/SetSysPort",
      method: 'post',
      data: data
    })
  }

  //刷新蜂窝网络状态
  exports.refreshCellularNet = function (data) {
    return axios({
      url: "/ITS/Network/GetMobNetStatus",
      method: 'post',
      data: data
    })
  }

  //获取蜂窝网络信息
  exports.getCellularNetwork = function (data) {
    return axios({
      url: "/ITS/Network/GetMobileNetwork",
      method: 'post',
      data: data
    })
  }

  //设置蜂窝网络信息
  exports.setCellularNetwork = function (data) {
    return axios({
      url: "/ITS/Network/SetMobileNetwork",
      method: 'post',
      data: data
    })
  }

  //获取wifi
  exports.getWifiConfig = function (data) {
    return axios({
      url: "/ITS/Network/GetWifiNetwork",
      method: 'post',
      data: data
    })
  }

  //设置wifi
  exports.setWifiConfig = function (data) {
    return axios({
      url: "/ITS/Network/SetWifiNetwork",
      method: 'post',
      data: data
    })
  }

  //获取GB28281Device 
  exports.getGB28281Device = function (data) {
    return axios({
      url: "/ITS/Network/GetGB28281Device",
      method: 'post',
      data: data
    })
  }

  //设置GB28281Device 
  exports.setGB28281Device = function (data) {
    return axios({
      url: "/ITS/Network/SetGB28281Device",
      method: 'post',
      data: data
    })
  }

  //获取GB28281Device 
  exports.getONVIFServer = function (data) {
    return axios({
      url: "/ITS/Network/GetOnvifServer",
      method: 'post',
      data: data
    })
  }

  //设置GB28281Device 
  exports.setONVIFServer = function (data) {
    return axios({
      url: "/ITS/Network/SetOnvifServer",
      method: 'post',
      data: data
    })
  }

})
