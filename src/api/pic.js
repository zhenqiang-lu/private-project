define(function (require, exports, module) {
    //基本信息获取
    exports.getPicParam = function (data) {
      return axios({
        url: "/ITS/Image/GetImageQuality",
        method: 'post',
        data: data
      })
    }
  
    //获取设备名称和设备编号
    exports.setPicParam = function (data) {
      return axios({
        url: "/ITS/Image/SetImageQuality",
        method: 'post',
        data: data
      })
    }
  
  })
  