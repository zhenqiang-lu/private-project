define(function(require, exports, module) {
    require('./menu.css');

    var toDate = require('../../tools/tools.js').toDate;

    module.exports = Vue.component('Menu', {
        template: ['<div class="view-box">',
            '  <el-container style="height: 100%;">',
            '    <el-header style="text-align: left; color: white; background: #567; padding-left: 0">',
            '      <p :class="isCollapse ? style.width64 : style.width160 " style="text-align: center; display: inline-block; background: #404d59">{{ isCollapse ? $t(\'admin.admin\') : $t(\'admin.system\') }}</p>',
            '      <i @click="flodFunc(true)"v-if="!isCollapse" class="el-icon-s-fold collapse-icon"></i>',
            '      <i @click="flodFunc(false)" v-else class="el-icon-s-unfold collapse-icon"></i>',
            '      <el-dropdown style="color: white; float: right" trigger="click">',
            '        <span class="el-dropdown-link">',
            '          {{ username }}<i class="el-icon-arrow-down el-icon--right"></i>',
            '        </span>',
            '        <el-dropdown-menu slot="dropdown">',
            '          <el-dropdown-item><span class="quit-span" @click="logout"><i class="el-icon-remove-outline"></i>{{ $t("common.quit") }}</span></el-dropdown-item>',
            '        </el-dropdown-menu>',
            '      </el-dropdown>',
            '      <span style="float: right; padding-right: 40px; padding-top: 2px; font-size: 14px">{{ nowTime }}</span>',
            '    </el-header>',
            '    <el-container>',
            '      <el-aside width="auto" style="overflow: hidden; background-color: #20262d">',
            '        <el-menu :collapse-transition="false" :class="isCollapse ? style.width64 : style.width160 " :collapse="isCollapse" :unique-opened="true" :default-active="num" class="el-menu-vertical-demo" text-color="#999999" background-color="#20262d" active-text-color="#FFFFFF">',
            '          <template v-for="(item, index) in menuList">',
            '            <el-submenu class="menu-li" v-if="item.children && item.children.length > 0" :index="item.num">',
            '              <template slot="title">',
            '                <i :class="item.icon"></i>',
            '                <span>{{ $t("menu." + item.name) }}</span>',
            '              </template>',
            '              <template v-for="(item2, index2) in item.children">',
            '                <el-menu-item :index="item2.num" class="menu-li" @click="goPage(item2)" >{{ $t("menu." + item2.name) }}</el-menu-item>',
            '              </template>',
            '            </el-submenu>',
            '            <el-menu-item class="menu-li" @click="goPage(item)" v-else :index="item.num">',
            '              <i :class="item.icon"></i>',
            '              <span slot="title">{{ $t("menu." + item.name) }}</span>',
            '            </el-menu-item>',
            '          </template>',
            '        </el-menu>',
            '      </el-aside>',
            '      <el-main>',
            '        <router-view style="background-color: white;padding: 20px;border-radius: 10px;box-shadow: 0 0 0 10px #EDEDED;"></router-view>',
            '        <p style="position: initial;" class="login-bottom">2020 Shenzhen XXXX Technology Co, Ltd. All Rights Reserverd.</p>',
            '      </el-main>',
            
            '    </el-container>',
            '  </el-container>',
            '</div>',
        ].join(""),
        data: function() { // 数据
            return {
                nowTime: '',
                style: {
                    width64: "width64",
                    width160: "width160"
                },
                isCollapse: false,
                username: "admin",
                num: this.$route.query.num || "1",
                menuList: [
                    {
                        name: 'system',
                        num: '2',
                        icon: 'el-icon-setting',
                        children: [{
                                path: '/admin/systemMsg',
                                name: 'setting',
                                num: '2-1',
                                children: false
                            },
                            {
                                name: 'defend',
                                num: '2-2',
                                path: '/admin/systemDefend',
                                children: false

                            },
                            {
                                name: 'user',
                                num: '2-3',
                                path: '/admin/systemUser',
                                children: false,

                            }
                        ]
                    },
                    {
                        name: 'net',
                        num: '3',
                        icon: 'el-icon-s-platform',
                        children: [{
                                name: 'basic',
                                num: '3-1',
                                children: false,
                                path: '/admin/netConfig',
                            },
                            {
                                name: 'high',
                                num: '3-2',
                                children: false,
                                path: '/admin/netHighConfig',
                            }
                        ]
                    },
                    {
                        name: 'media',
                        num: '4',
                        icon: 'el-icon-video-camera',
                        children: [{
                                name: 'parameter',
                                path: '/admin/video',
                                num: '4-1',
                                children: false
                            },
                        ]
                    },
                    {
                        name: 'picture',
                        num: '5',
                        icon: 'el-icon-picture-outline',
                        children: false,
                        path: '/admin/pictureShow',
                        // children: [{
                        //     name: 'show',
                            
                        //     num: '5-1',
                        //     children: false
                        // }]
                    },
                    {
                        name: 'memory',
                        num: '6',
                        icon: 'el-icon-s-order',
                        path: '/admin/localDisk',
                        children: false
                    }
                ]
            }
        },
        created: function () {
            sessionStorage.setItem('isCollapse', this.isCollapse);
            this.goTime()
        },
        methods: {
            goTime: function () {
                var _this = this;
                setInterval(function () {
                    _this.nowTime = toDate(new Date().getTime())
                }, 1000)
            },
            goPage: function(data) {
                if (data.path === this.$route.path) return
                this.$router.push({
                    path: data.path,
                    query: {
                        num: data.num
                    }
                })
            },
            flodFunc: function() {
                this.isCollapse = !this.isCollapse
                sessionStorage.setItem('isCollapse', this.isCollapse)
            },
            logout: function() {
                this.$router.push({
                    path: "/"
                })
            }
        }
    })
})
