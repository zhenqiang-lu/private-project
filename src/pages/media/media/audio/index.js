define(function (require, exports, module) {
    require('./index.css');

    var getAencParam = require('../../../../api/media.js').getAencParam;
    var setAencParam = require('../../../../api/media.js').setAencParam;

    module.exports = Vue.component('index', {
        template: ['<div>',
            '  <el-tabs v-model="activeName"  @tab-click="tabClick">',
            '    <el-tab-pane :label="$t(\'media.video\')" name="first"></el-tab-pane>',
            '    <el-tab-pane :label="$t(\'media.audio\')" name="second">',
            '      <div class="basic-item-box">',
            '        <label for="">{{ $t(\'common.enable\') }}</label>',
            '        <el-checkbox v-model="audio.Enable" :true-label="1" :false-label="0" style="width: 480px; text-align: left;"></el-checkbox>',
            '      </div>',
            '      <div class="basic-item-box">',
            '        <label for="">{{ $t(\'media.Format\') }}</label>',
            '        <el-select :disabled=" audio.Enable === 0 " class="input-box" size="mini" v-model="audio.Format">',
            '          <el-option label="G.726" :value="0"></el-option>',
            '          <el-option label="AAC" :value="1"></el-option>',
            '        </el-select>',
            '      </div>',
            '      <div class="basic-item-box">',
            '        <label for="">{{ $t(\'media.Input\') }}</label>',
            '        <el-select :disabled=" audio.Enable === 0 " class="input-box" size="mini" v-model="audio.Input">',
            '          <el-option label="MICIN" :value="0"></el-option>',
            '          <el-option label="LINEIN" :value="1"></el-option>',
            '        </el-select>',
            '      </div>',
            '      <div class="basic-item-box" :class="audioSlider">',
            '        <label for="">{{ $t(\'media.Volume\') }}</label>',
            '        <el-slider v-model="audio.Volume" :disabled=" audio.Enable === 0 " style="width: 480px; display: inline-block; vertical-align: middle;" :show-input-controls="false" input-size="mini" show-input></el-slider>',
            '      </div>',
            '      <div style="text-align: center; margin-top: 10px;">',
            '        <el-button @click="setAencParam" icon="el-icon-receiving" size="mini" type="success">{{ $t(\'common.save\') }}</el-button>',
            '      </div>',
            '    </el-tab-pane>',
            '    <el-tab-pane :label="$t(\'media.osd\')" name="third"></el-tab-pane>',
            '  </el-tabs>',
            '</div>'
        ].join(""),
        data: function () { // 数据
            return {
                activeName: 'second',
                audio: {
                    Enable: 1,
                    Format: 0,
                    Input: 0,
                    Volume: 0,
                }
            }
        },
        created: function () {
            this.getAencParam();
        },
        computed: {
            audioSlider: function () {
                // var rangeVolume = {
                //     '0-30': 'audio-slider0',
                //     '30-60': 'audio-slider1',
                //     '60-100': 'audio-slider2'
                // }
                // for(var key in rangeVolume) {
                //     var ran_val = key.split('-');
                //     if(this.audio.Volume >= ran_val[0] && this.audio.Volume< ran_val[1]) {
                //         return rangeVolume[key];
                //     }
                // }
                return 'audio-slider0';
            }
        },
        methods: {
            getAencParam: function () {
                var _this = this;
                getAencParam({}).then(function (res) {
                    var data = res.data;
                    if (data.code == 1000) {
                        _this.audio = data.data;
                    } else {
                        _this.$message.error("音频参数获取失败")
                    }
                }).catch(function (res) {
                    _this.$message.error("获取音频参数请求失败")
                    return;
                })
            },
            setAencParam: function () {
                var _this = this;
                setAencParam(_this.audio).then(function (res) {
                    var data = res.data;
                    if (data.code == 1000) {
                        _this.$message.success("保存成功");
                        _this.getAencParam();
                    } else {
                        _this.$message.error("保存失败")
                    }
                }).catch(function (res) {
                    _this.$message.error("请求失败")
                    return;
                })
            },
            tabClick: function () {
                var enumObj = {
                    first: '/admin/video',
                    second: '/admin/audio',
                    third: '/admin/osdConfig'
                }
                if (enumObj[this.activeName] && enumObj[this.activeName] !== this.$route.path) {
                    this.$router.push({
                        path: enumObj[this.activeName],
                        query: {
                            num: this.$route.query.num
                        }
                    })
                }
                return
            },
        }
    })
})
