define(function (require, exports, module) {
    require('./index.css');

    var getOSDConfig = require('../../../../api/media.js').getOSDConfig;
    var setOSDConfig = require('../../../../api/media.js').setOSDConfig;

    module.exports = Vue.component('index', {
        template: ['<div>',
            '  <el-tabs @tab-click="tabClick" v-model="activeName">',
            '    <el-tab-pane :label="$t(\'media.video\')" name="first"></el-tab-pane>',
            '    <el-tab-pane :label="$t(\'media.audio\')" name="second"></el-tab-pane>',
            '    <el-tab-pane border :label="$t(\'media.osd\')" name="third">',
            '      <el-row>',
            '        <el-col :span="12">',
            // '             <live-player muted="false"  video-url="http://192.168.31.117/myapp/yequ.mp4" mode="RTC" live="true"></live-player>',
            '          <div id="dplayer" style="width:100%; margin: 0 auto;">',
            '            <video id="videoElement" autoplay controls width="100%"></video>',
            '          </div>',
            // '             <div id="flv" style="height: 480px; width: 640px; margin: 0 auto;"></div>',
            '        </el-col>',
            '        <el-col style="text-align: left;" :span="12">',
            '          <div class="pic-item">',
            '            <label for="">{{ $t(\'media.DispalyName\') }}</label>',
            '            <el-checkbox :true-label="1" :false-label="0" style="width: 400px; text-align: left;" v-model="OSDSet.DispalyName"></el-checkbox>',
            '          </div> ',
            '          <div class="pic-item">',
            '            <label for="">{{ $t(\'media.DispalyDate\') }}</label>',
            '            <el-checkbox :true-label="1" :false-label="0" style="width: 400px; text-align: left;" v-model="OSDSet.DispalyDate"></el-checkbox>',
            '          </div> ',
            '          <div class="pic-item">',
            '            <label for="">{{ $t(\'media.DispalyWeek\') }}</label>',
            '            <el-checkbox :true-label="1" :false-label="0" style="width: 400px; text-align: left;" v-model="OSDSet.DispalyWeek"></el-checkbox>',
            '          </div> ',
            '          <div class="pic-item">',
            '            <label for="">{{ $t(\'media.ChannelName\') }}</label>',
            '            <el-input class="pic-input" size="mini" v-model="OSDSet.ChannelName"></el-input>',
            '          </div>',
            '          <div class="pic-item">',
            '            <label for="">{{ $t(\'media.TimeFormat\') }}</label>',
            '            <el-select class="pic-input" size="mini" v-model="OSDSet.TimeFormat">',
            '              <el-option :label="$t(\'media._12Hours\')" :value="0"></el-option>',
            '              <el-option :label="$t(\'media._24Hours\')" :value="1"></el-option>',
            '            </el-select>',
            '          </div>',
            '          <div class="pic-item">',
            '            <label for="">{{ $t(\'media.DataFormat\') }}</label>',
            '            <el-select class="pic-input" size="mini" v-model="OSDSet.DataFormat">',
            '              <el-option label="YYYY-MM-DD" :value="0"></el-option>',
            '              <el-option label="YYYY/MM/DD" :value="1"></el-option>',
            '            </el-select>',
            '          </div>',
            '          <div class="pic-item">',
            '            <label for="">{{ $t(\'media.Attribute\') }}</label>',
            '            <el-select class="pic-input" size="mini" v-model="OSDSet.Attribute">',
            '              <el-option :label="$t(\'media.transparent_twinkle\')" :value="0"></el-option>',
            '              <el-option :label="$t(\'media.transparent_unTwinkle\')" :value="1"></el-option>',
            '              <el-option :label="$t(\'media.unTransparent_twinkle\')" :value="2"></el-option>',
            '              <el-option :label="$t(\'media.unTransparent_unTwinkle\')" :value="3"></el-option>',
            '            </el-select>',
            '          </div>',
            '          <div class="pic-item">',
            '            <label for="">{{ $t(\'media.Fonts\') }}</label>',
            '            <el-select class="pic-input" size="mini" v-model="OSDSet.Fonts">',
            '              <el-option :label="$t(\'common.auto\')" :value="0"></el-option>',
            '            </el-select>',
            '          </div>',
            '          <div class="pic-item">',
            '              <label style="vertical-align: top;" for="">{{ $t(\'media.Colour\') }}</label>',
            '              <el-select v-show="showColorPicker" class="pic-input" size="mini" v-model="OSDSet.Colour">',
            // '               <el-option :label="$t(\'media.blackWhite\')" :value="0"></el-option>',
            '                  <el-option :label="$t(\'media.black\')" value="#000000"></el-option>',
            '                  <el-option :label="$t(\'media.white\')" value="#FFFFFF"></el-option>',
            '                  <el-option :label="$t(\'media.custom\')" value=""></el-option>',
            '              </el-select>',
            '              <el-color-picker v-model="OSDSet.Colour" v-show="!showColorPicker" class="pic-input"></el-color-picker>',
            '          </div>',
            // '          <div class="pic-item">',
            // '            <label style="vertical-align: top;" for="">OSD颜色</label>',
            // '            <el-color-picker style="width: 400px; text-align: left;" v-model="OSDSet.Colour"></el-color-picker>',
            // '          </div>',
            '          <div class="pic-item">',
            '            <label for="">{{ $t(\'media.Alignment\') }}</label>',
            '            <el-select class="pic-input" size="mini" v-model="OSDSet.Alignment">',
            '              <el-option :label="$t(\'media.left\')" :value="0"></el-option>',
            '              <el-option :label="$t(\'media.right\')" :value="1"></el-option>',
            '            </el-select>',
            '          </div>',
            '          <div style="margin-top: 10px; text-indent: 240px">',
            '            <el-button @click="setOSDConfig" icon="el-icon-receiving" size="mini" type="success">{{ $t(\'common.save\') }}</el-button>',
            '          </div>',
            '        </el-col>',
            '      </el-row>',
            '    </el-tab-pane>',
            '  </el-tabs>',
            '</div>',
        ].join(""),
        data: function () { // 数据
            return {
                flvPlayer: null,
                activeName: 'third',
                OSDSet: {
                    DispalyName: 0,
                    DispalyDate: 0,
                    DispalyWeek: 0,
                    ChannelName: "",
                    TimeFormat: 0,
                    DataFormat: 0,
                    Attribute: 0,
                    Fonts: 0,
                    Colour: '#000000',
                    Alignment: 0
                },
            }
        },
        created: function () {
            this.getOSDConfig();
        },
        mounted: function () {
            if (flvjs.isSupported()) {
                var videoElement = document.getElementById('videoElement')
                this.flvPlayer = flvjs.createPlayer({
                    type: 'flv',
                    cors: true,
                    isLive: true,
                    url: 'http://192.168.31.117/myapp/yequ.mp4'
                    // url: 'https://stream7.iqilu.com/10339/upload_transcode/202002/18/20200218114723HDu3hhxqIT.mp4'
                }, {
                    stashInitialSize: '1200KB',
                    enableStashBuffer: false
                });
                this.flvPlayer.attachMediaElement(videoElement);
                this.flvPlayer.load()
                this.flvPlayer.play()
            }

        },
        computed: {
            showColorPicker: function () {
                return ['#FFFFFF', '#000000'].some(function (item) {
                    return this.OSDSet ? item === this.OSDSet.Colour : false
                });
            }
        },
        methods: {
            play: function () {
                this.flvPlayer.play();
            },
            getOSDConfig: function () {
                var _this = this;
                getOSDConfig({}).then(function (res) {
                    var data = res.data;
                    if (data.code == 1000) {
                        _this.OSDSet = data.data;
                    } else {
                        _this.$message.error("OSD设置获取失败")
                    }
                }).catch(function (res) {
                    _this.$message.error("获取OSD设置请求失败")
                    return;
                })
            },
            setOSDConfig: function () {
                var _this = this;
                setOSDConfig(_this.OSDSet).then(function (res) {
                    var data = res.data;
                    if (data.code == 1000) {
                        _this.$message.success("保存成功");
                        _this.getOSDConfig();
                    } else {
                        _this.$message.error("保存失败")
                    }
                }).catch(function (res) {
                    _this.$message.error("请求失败")
                    return;
                })
            },
            tabClick: function () {
                var enumObj = {
                    first: '/admin/video',
                    second: '/admin/audio',
                    third: '/admin/osdConfig'
                }
                if (enumObj[this.activeName] && enumObj[this.activeName] !== this.$route.path) {
                    this.$router.push({
                        path: enumObj[this.activeName],
                        query: {
                            num: this.$route.query.num
                        }
                    })
                }
                return
            },
        }
    })
})
