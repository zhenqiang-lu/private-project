define(function (require, exports, module) {
    require('./index.css');

    var getVencParam = require('../../../../api/media.js').getVencParam;
    var setVencParam = require('../../../../api/media.js').setVencParam;

    module.exports = Vue.component('index', {
        template: ['<div>',
            '  <el-tabs v-model="activeName"  @tab-click="tabClick">',
            '    <el-tab-pane :label="$t(\'media.video\')" name="first">',
            '      <el-row>',
            '        <el-col :span="12">',
            '          <div class="basic-item-box">',
            '            <label for="">{{ $t(\'media.BitType\') }}</label>',
            '            <el-input disabled class="media-input-box" size="mini" :value="$t(\'media.Main\')"></el-input>',
            '          </div>',
            '          <div class="basic-item-box">',
            '            <label for="">{{ $t(\'media.VencFormat\') }}</label>',
            '            <el-select class="media-input-box" size="mini" v-model="video.Main.Format">',
            '              <el-option label="H264" :value="0"></el-option>',
            '              <el-option label="H265" :value="1"></el-option>',
            '            </el-select>',
            '          </div>',
            '          <div class="basic-item-box">',
            '            <label for="">{{ $t(\'media.BitRateMode\') }}</label>',
            '            <el-select class="media-input-box" size="mini" v-model="video.Main.BitRateMode">',
            '              <el-option label="CBR" :value="0"></el-option>',
            '              <el-option label="VBR" :value="1"></el-option>',
            '            </el-select>',
            '          </div>',
            '          <div class="basic-item-box">',
            '            <label for="">{{ $t(\'media.Resolution\') }}</label>',
            '            <el-select class="media-input-box" size="mini" v-model="video.Main.Resolution">',
            '              <el-option v-for="(item, index) in ResolutionList" :key="index" :label="item.label" :value="item.value"></el-option>',
            '            </el-select>',
            '          </div>',
            '          <div class="basic-item-box">',
            '            <label for="">{{ $t(\'media.FrameRate\') }}</label>',
            '            <el-select class="media-input-box" size="mini" v-model="video.Main.FrameRate">',
            '              <el-option v-for="(item, index) in FrameRateList" :key="index" :label="item" :value="item"></el-option>',
            '            </el-select>',
            '          </div>',
            '          <div class="basic-item-box">',
            '            <label for="">{{ $t(\'media.Quality\') }}</label>',
            '            <el-select class="media-input-box" size="mini" v-model="video.Main.Quality">',
            '              <el-option :label="$t(\'media.Worst\')" :value="0"></el-option>',
            '              <el-option :label="$t(\'media.Normal\')" :value="1"></el-option>',
            '              <el-option :label="$t(\'media.Good\')" :value="2"></el-option>',
            '              <el-option :label="$t(\'media.Better\')" :value="3"></el-option>',
            '              <el-option :label="$t(\'media.Best\')" :value="4"></el-option>',
            '            </el-select>',
            '          </div>',
            '          <div class="basic-item-box">',
            '            <label for="">{{ $t(\'media.BitRate\') }}</label>',
            '            <el-input class="media-input-box" size="mini" v-model="video.Main.BitRate">',
            '              <template slot="append">(Kbit)(128-8192)</template>',
            '            </el-input>',
            '          </div>',
            '          <div class="basic-item-box">',
            '            <label for="">{{ $t(\'media.GopSize\') }}</label>',
            '            <el-input class="media-input-box" size="mini" v-model="video.Main.GopSize">',
            '              <template slot="append">(FPS)(1-50)</template>',
            '            </el-input>',
            '          </div>',
            '        </el-col>',
            '        <el-col :span="12">',
            '          <div class="basic-item-box">',
            '            <label for="">{{ $t(\'media.BitType\') }}</label>',
            '            <el-input disabled class="media-input-box" size="mini" :value="$t(\'media.Sub\')"></el-input>',
            '          </div>',
            '          <div class="basic-item-box">',
            '            <label for="">{{ $t(\'media.VencFormat\') }}</label>',
            '            <el-select class="media-input-box" size="mini" v-model="video.Sub.Format">',
            '              <el-option label="H264" :value="0"></el-option>',
            '              <el-option label="H265" :value="1"></el-option>',
            '            </el-select>',
            '          </div>',
            '          <div class="basic-item-box">',
            '            <label for="">{{ $t(\'media.BitRateMode\') }}</label>',
            '            <el-select class="media-input-box" size="mini" v-model="video.Sub.BitRateMode">',
            '              <el-option label="CBR" :value="0"></el-option>',
            '              <el-option label="VBR" :value="1"></el-option>',
            '            </el-select>',
            '          </div>',
            '          <div class="basic-item-box">',
            '            <label for="">{{ $t(\'media.Resolution\') }}</label>',
            '            <el-select class="media-input-box" size="mini" v-model="video.Sub.Resolution">',
            '              <el-option v-for="(item, index) in ResolutionList" :key="index" :label="item.label" :value="item.value"></el-option>',
            '            </el-select>',
            '          </div>',
            '          <div class="basic-item-box">',
            '            <label for="">{{ $t(\'media.FrameRate\') }}</label>',
            '            <el-select class="media-input-box" size="mini" v-model="video.Sub.FrameRate">',
            '              <el-option v-for="(item, index) in FrameRateList" :key="index" :label="item" :value="item"></el-option>',
            '            </el-select>',
            '          </div>',
            '          <div class="basic-item-box">',
            '            <label for="">{{ $t(\'media.Quality\') }}</label>',
            '            <el-select class="media-input-box" size="mini" v-model="video.Sub.Quality">',
            '              <el-option :label="$t(\'media.Worst\')" :value="0"></el-option>',
            '              <el-option :label="$t(\'media.Normal\')" :value="1"></el-option>',
            '              <el-option :label="$t(\'media.Good\')" :value="2"></el-option>',
            '              <el-option :label="$t(\'media.Better\')" :value="3"></el-option>',
            '              <el-option :label="$t(\'media.Best\')" :value="4"></el-option>',
            '            </el-select>',
            '          </div>',
            '          <div class="basic-item-box">',
            '            <label for="">{{ $t(\'media.BitRate\') }}</label>',
            '            <el-input class="media-input-box" size="mini" v-model="video.Sub.BitRate">',
            '              <template slot="append">(Kbit)(128-8192)</template>',
            '            </el-input>',
            '          </div>',
            '          <div class="basic-item-box">',
            '            <label for="">{{ $t(\'media.GopSize\') }}</label>',
            '            <el-input class="media-input-box" size="mini" v-model="video.Sub.GopSize">',
            '              <template slot="append">(FPS)(1-50)</template>',
            '            </el-input>',
            '          </div>',
            '        </el-col>',
            '      </el-row>',
            '      <div style="text-align: center; margin-top: 10px; margin-left: 60px">',
            '        <el-button @click="setVencParam" icon="el-icon-receiving" size="mini" type="success">{{ $t(\'common.save\') }}</el-button>',
            '      </div>',
            '    </el-tab-pane>',
            '    <el-tab-pane :label="$t(\'media.audio\')" name="second"></el-tab-pane>',
            '    <el-tab-pane :label="$t(\'media.osd\')" name="third"></el-tab-pane>',
            '  </el-tabs>',
            '</div>'
        ].join(""),
        data: function () { // 数据
            return {
                activeName: 'first',
                FrameRateList: [8, 10, 12, 15, 16, 18, 20, 22, 24, 25, 30, 50, 60],
                ResolutionList: [
                    {value: 0, label: 'D1'},
                    {value: 1, label: 'CIF'},
                    {value: 2, label: '960H'},
                    {value: 3, label: 'QCFI'},
                    {value: 4, label: 'XGA(1024*768)'},
                    {value: 5, label: '720P(1280*720)'},
                    {value: 6, label: 'SXGA(1280*1024)'},
                    {value: 7, label: '1080P(1920*1080)'},
                    {value: 8, label: '2K(2048*1536)'},
                    {value: 9, label: '4K(3840*2160)'},
                ],
                video: {
                    Main: {
                        Format: 0,  //编码类型
                        BitRate: 0,    //码流大小
                        FrameRate: 8,   //  帧率
                        BitRateMode: 0,   //码流模式
                        Quality: 0,   //图像质量
                        GopType: 0,
                        GopSize: 0,   //I帧间隔
                        Resolution: 0,   //视频大小
                        Profile: 0,
                        EncLib: 0
                    },
                    Sub: {
                        Format: 0,
                        BitRate: 0,
                        FrameRate: 8,
                        BitRateMode: 0,
                        Quality: 0,
                        GopType: 0,
                        GopSize: 0,
                        Resolution: 0,
                        Profile: 0,
                        EncLib: 0
                    }
                }
            }
        },
        created: function () {
            this.getVencParam();
        },
        methods: {
            getVencParam: function () {
                var _this = this;
                getVencParam({}).then(function (res) {
                    var data = res.data;
                    if (data.code == 1000) {
                        _this.video = data.data;
                    } else {
                        _this.$message.error("视频参数获取失败")
                    }
                }).catch(function (res) {
                    _this.$message.error("获取视频参数请求失败")
                    return;
                })
            },
            setVencParam: function () {
                var _this = this;
                setVencParam(_this.video).then(function (res) {
                    var data = res.data;
                    if (data.code == 1000) {
                        _this.$message.success("保存成功");
                        _this.getVencParam();
                    } else {
                        _this.$message.error("保存失败")
                    }
                }).catch(function (res) {
                    _this.$message.error("请求失败")
                    return;
                })
            },
            tabClick: function () {
                var enumObj = {
                    first: '/admin/video',
                    second: '/admin/audio',
                    third: '/admin/osdConfig'
                }
                if (enumObj[this.activeName] && enumObj[this.activeName] !== this.$route.path) {
                    this.$router.push({
                        path: enumObj[this.activeName],
                        query: {
                            num: this.$route.query.num
                        }
                    })
                }
                return
            },
        }
    })
})
