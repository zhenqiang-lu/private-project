define(function(require, exports, module) {
    require('./index.css');
    module.exports = Vue.component('index', {
        template: ['<div>',
            '    <el-tabs v-model="activeName">',
            '        <el-tab-pane :label="$t(\'disk.diskManager\')" name="first">',
            '            <p class="p-title">',
            '                <span>{{$t(\'disk.diskManager\')}}</span>',
            '                <el-button class="right" style="margin: 6px 10px;" size="mini" type="danger">{{$t(\'disk.formatting\')}}</el-button>',
            '            </p>',
            '            <el-table :data="tableData" highlight-current-row border tooltip-effect="dark" style="width: 100%; float: left" @selection-change="handleSelectionChange">',
            '                <el-table-column type="selection"></el-table-column>',
            '                <el-table-column align="center" :label="$t(\'disk.diskNumber\')">',
            '                    <template slot-scope="scope">{{ scope.row.name }}</template>',
            '                </el-table-column>',
            '                <el-table-column align="center" :label="$t(\'disk.size\')">',
            '                    <template slot-scope="scope">{{ scope.row.name }}</template>',
            '                </el-table-column>',
            '                <el-table-column align="center" :label="$t(\'disk.sizeLeft\')">',
            '                    <template slot-scope="scope">{{ scope.row.name }}</template>',
            '                </el-table-column>',
            '                <el-table-column align="center" :label="$t(\'disk.status\')">',
            '                    <template slot-scope="scope">{{ scope.row.name }}</template>',
            '                </el-table-column>',
            '                <el-table-column align="center" :label="$t(\'disk.type\')">',
            '                    <template slot-scope="scope">{{ scope.row.name }}</template>',
            '                </el-table-column>',
            '                <el-table-column align="center" :label="$t(\'disk.props\')">',
            '                    <template slot-scope="scope">{{ scope.row.name }}</template>',
            '                </el-table-column>',
            '                <el-table-column align="center" :label="$t(\'disk.loads\')">',
            '                    <template slot-scope="scope">{{ scope.row.name }}</template>',
            '                </el-table-column>',
            '            </el-table>',
            // '            <div style="text-align: center; margin-top: 10px;">',
            // '                <el-button icon="el-icon-receiving" size="mini" type="success">保存</el-button>',
            // '            </div>',
            '        </el-tab-pane>',
            '    </el-tabs>',
            '</div>',
        ].join(""),
        data: function() { // 数据
            return {
                activeName: 'first',
                tableData: [],
                value: ''
            }
        },
        mounted: function () {
            this.tableData = [
                {
                    name: 'name'
                },
            ]
        },
        methods: {
            handleSelectionChange: function(val) {

            }
        }
    })
})
