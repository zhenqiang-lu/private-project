define(function (require, exports, module) {
    require('./index.css');

    var refreshCellularNet = require('../../../../api/network.js').refreshCellularNet;
    var getCellularNetwork = require('../../../../api/network.js').getCellularNetwork;
    var setCellularNetwork = require('../../../../api/network.js').setCellularNetwork;

    module.exports = Vue.component('index', {
        template: ['<div>',
            '  <el-tabs v-model="activeName" @tab-click="tabClick">',
            '    <el-tab-pane label="TCP/IP" name="first">TCPIP</el-tab-pane>',
            '    <el-tab-pane :label="$t(\'network.port\')" name="third">PORT </el-tab-pane>',
            '    <el-tab-pane :label="$t(\'network.mobile\')" name="fifth">',
            '      <div class="port-wrap">',
            '        <div>',
            '          <h4 class="wireless-title">{{ $t(\'network.dialStatus\') }}</h4>',
            '          <div class="split-line"></div>',
            '          <div class="net-item-box">',
            '            <label for="">{{ $t(\'network.RealTimeMode\') }}</label>',
            '            <el-input disabled class="port-input" size="mini" v-model="dialStatus.RealTimeModel"></el-input>',
            '          </div>',
            '          <div class="net-item-box">',
            '            <label for="">{{ $t(\'network.UIMStatus\') }}</label>',
            '            <el-input disabled class="port-input" size="mini" v-model="dialStatus.UIMStatus"></el-input>',
            '          </div>',
            '          <div class="net-item-box">',
            '            <label for="">{{ $t(\'network.Signal\') }}</label>',
            '            <el-input disabled class="port-input" size="mini" v-model="dialStatus.Signal"></el-input>',
            '          </div>',
            '          <div class="net-item-box">',
            '            <label for="">{{ $t(\'network.dialStatus\') }}</label>',
            '            <el-input disabled class="port-input" size="mini" v-model="dialStatus.DialStatus"></el-input>',
            '          </div>',
            '          <div class="net-item-box">',
            '            <label for="">{{ $t(\'network.IPAddress\') }}</label>',
            '            <el-input disabled class="port-input" size="mini" v-model="dialStatus.IPAddress"></el-input>',
            '          </div>',
            '          <div class="net-item-box">',
            '            <label for="">{{ $t(\'network.Mask\') }}</label>',
            '            <el-input disabled class="port-input" size="mini" v-model="dialStatus.Mask"></el-input>',
            '          </div>',
            '          <div class="net-item-box">',
            '            <label for="">{{ $t(\'network.Gateway\') }}</label>',
            '            <el-input disabled class="port-input" size="mini" v-model="dialStatus.Gateway"></el-input>',
            '          </div>',
            '          <div class="net-item-box">',
            '            <label for="">{{ $t(\'network.dialStatus\') }}</label>',
            '            <el-input disabled class="port-input" size="mini" v-model="dialStatus.DNS"></el-input>',
            '          </div>',
            '          <div class="net-item-box">',
            '            <label for="">{{ $t(\'network.PhoneNum\') }}</label>',
            '            <el-input disabled class="port-input" size="mini" v-model="dialStatus.Mobile"></el-input>',
            '          </div>',
            '          <div style="text-align: center; margin-top: 10px; padding-left: 120px;">',
            '            <el-button @click="refreshCellularNet" size="mini" type="success">{{ $t(\'common.fresh\') }}</el-button>',
            '          </div>',
            '        </div>',
            '        <div>',
            '          <h4 class="wireless-title">{{ $t(\'network.dialParam\') }}</h4>',
            '          <div class="split-line"></div>',
            '          <div class="net-item-box">',
            '            <label>{{ $t(\'common.enable\') }}</label>',
            '            <el-checkbox v-model="dialParam.Enable" :true-label="1" :false-label="0" style="width: 360px; text-align: left;"></el-checkbox>',
            '          </div>',
            '          <div class="net-item-box">',
            '            <label for="">{{ $t(\'network.DialMode\') }}</label>',
            '            <el-select class="port-input" size="mini" v-model="dialParam.DialModel">',
            '               <el-option :label="$t(\'common.auto\')" :value="0"></el-option>',
            '               <el-option :label="$t(\'common.hand\')" :value="1"></el-option>', 
            '            </el-select>',
            '          </div>',
            '          <div class="net-item-box">',
            '            <label for="">{{ $t(\'network.NetSwitch\') }}</label>',
            '            <el-select class="port-input" size="mini" v-model="dialParam.NetSwitch">',
            '               <el-option :label="$t(\'common.auto\')" :value="0"></el-option>',
            '               <el-option :label="$t(\'common.empty\')" :value="1"></el-option>',
            '               <el-option label="2G" :value="2"></el-option>',
            '               <el-option label="3G" :value="3"></el-option>',
            '               <el-option label="4G" :value="4"></el-option>',
            '               <el-option label="5G" :value="5"></el-option>',
            '            </el-select>',
            '          </div>',
            '          <div class="net-item-box">',
            '            <label for="">{{ $t(\'network.OfflineTime\') }}</label>',
            '            <el-select :disabled="dialParam.DialModel === 0" class="port-input" size="mini" v-model="dialParam.OfflineTime">',
            '               <el-option label="1" :value="0"></el-option>',
            '               <el-option label="60" :value="1"></el-option>',
            '               <el-option label="200" :value="2"></el-option>',
            '               <el-option label="300" :value="3"></el-option>',
            '               <el-option label="500" :value="4"></el-option>',
            '               <el-option label="600" :value="5"></el-option>',
            '            </el-select>',
            '          </div>',
            '          <div class="net-item-box">',
            '            <label for="">{{ $t(\'message.username\') }}</label>',
            '            <el-input class="port-input" size="mini" v-model="dialParam.UserName"></el-input>',
            '          </div>',
            '          <div class="net-item-box">',
            '            <label for="">{{ $t(\'message.password\') }}</label>',
            '            <el-input class="port-input" :type="viewPwd ? \'text\' : \'password\' " size="mini" v-model="dialParam.Password">',
            '              <i @click=" viewPwd = !viewPwd " class="el-icon-view el-input__icon" slot="suffix"></i>',
            '            </el-input>',
            '          </div>',
            '          <div class="net-item-box">',
            '            <label for="">APN</label>',
            '            <el-input class="port-input" size="mini" v-model="dialParam.APN"></el-input>',
            '          </div>',
            '          <div class="net-item-box">',
            '            <label for="">MTU</label>',
            '            <el-input class="port-input" size="mini" v-model="dialParam.MTU"></el-input>',
            '          </div>',
            '          <div class="net-item-box">',
            '            <label for="">{{ $t(\'network.VftProc\') }}</label>',
            '            <el-select class="port-input" size="mini" v-model="dialParam.VftProc">',
            '               <el-option :label="$t(\'common.auto\')" :value="0"></el-option>',
            '            </el-select>',
            '          </div>',
            '          <div style="text-align: center; margin-top: 10px; padding-left: 120px;">',
            '            <el-button @click="setCellularNetwork" size="mini" type="success">{{ $t(\'common.save\') }}</el-button>',
            '          </div>',
            '        </div>',
            '      </div>',
            '    </el-tab-pane>',
            '    <el-tab-pane :label="$t(\'network.wificonfig\')" name="sixth">WIFI</el-tab-pane>',
            '  </el-tabs>',
            '  ',
            '</div>'
        ].join(""),
        data: function () {
            return {
                activeName: 'fifth',
                viewPwd: false,
                //拨号状态
                dialStatus: {
                    RealTimeModel: "",
                    UIMStatus: "",
                    Mobile: "",
                    Signal: 0,
                    DialStatus: "",
                    IPAddress: "",
                    Mask: "",
                    Gateway: "",
                    DNS: ""
                },
                // 拨号参数
                dialParam: {
                    Enable: 0,
                    DialModel: 0,
                    NetSwitch: 0,
                    OfflineTime: 0,
                    UserName: "",
                    Password: "",
                    APN: "",
                    MTU: 0,
                    VftProc: 0
                },
            }
        },
        created: function () {
            this.refreshCellularNet();
            this.getCellularNetwork();
        },
        methods: {
            tabClick: function () {
                var enumObj = {
                    first: '/admin/netConfig',
                    third: '/admin/netPort',
                    fifth: '/admin/netMobile',
                    sixth: '/admin/netWifi'
                }
                if(enumObj[this.activeName] && enumObj[this.activeName] !== this.$route.path) {
                    this.$router.push({
                        path: enumObj[this.activeName],
                        query: {
                            num: this.$route.query.num
                        }
                    })
                }
                return
            },
            refreshCellularNet: function () {
                var _this = this;
                refreshCellularNet({}).then(function (res) {
                    var data = res.data;
                    if (data.code == 1000) {
                        _this.dialStatus = data.data;
                        _this.$message.success("刷新成功")
                    } else {
                        _this.$message.error("刷新失败")
                    }
                }).catch(function (res) {
                    _this.$message.error("刷新请求失败")
                    return;
                })
            },
            getCellularNetwork: function () {
                var _this = this;
                getCellularNetwork({}).then(function (res) {
                    var data = res.data;
                    if (data.code == 1000) {
                        _this.dialParam = data.data;
                    } else {
                        _this.$message.error("拨号参数获取失败")
                    }
                }).catch(function (res) {
                    _this.$message.error("获取拨号参数请求失败")
                    return;
                })
            },
            setCellularNetwork: function () {
                var _this = this;
                setCellularNetwork(_this.dialParam).then(function (res) {
                    var data = res.data;
                    if (data.code == 1000) {
                        _this.$message.success("保存成功");
                        _this.getCellularNetwork();
                    } else {
                        _this.$message.error("保存失败")
                    }
                }).catch(function (res) {
                    _this.$message.error("请求失败")
                    return;
                })
            },
        }
    })
})
