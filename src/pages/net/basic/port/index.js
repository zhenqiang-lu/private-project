define(function (require, exports, module) {
    require('./index.css');

    var getSysPort = require('../../../../api/network.js').getSysPort;
    var setSysPort = require('../../../../api/network.js').setSysPort;

    module.exports = Vue.component('index', {
        template: ['<div>',
            '  <el-tabs v-model="activeName" @tab-click="tabClick">',
            '    <el-tab-pane label="TCP/IP" name="first">TCPIP</el-tab-pane>',
            '    <el-tab-pane :label="$t(\'network.port\')" name="third">',
            '      <div class="net-item-box">',
            '        <label for="">{{ $t("network.sdkPort") }}</label>',
            '        <el-input class="net-input" size="mini" v-model="port.SDKPort"></el-input>',
            '      </div>',
            '      <div class="net-item-box">',
            '        <label for="">{{ $t("network.httpPort") }}</label>',
            '        <el-input class="net-input" size="mini" v-model="port.HTTPPort"></el-input>',
            '      </div>',
            '      <div class="net-item-box">',
            '        <label for="">{{ $t("network.rtspPort") }}</label>',
            '        <el-input class="net-input" size="mini" v-model="port.RTSPPort"></el-input>',
            '      </div>',
            '      <div class="net-item-box">',
            '        <label for="">{{ $t("network.httpsPort") }}</label>',
            '        <el-input class="net-input" size="mini" v-model="port.HTTPSPort"></el-input>',
            '      </div>',
            '      <div style="text-align: center; margin-top: 10px;">',
            '        <el-button icon="el-icon-receiving" size="mini" type="success" @click="savePort">保存</el-button>',
            '      </div>',
            '    </el-tab-pane>',
            '    <el-tab-pane :label="$t(\'network.mobile\')" name="fifth">MOBILE</el-tab-pane>',
            '    <el-tab-pane :label="$t(\'network.wificonfig\')" name="sixth">WIFI</el-tab-pane>',
            '  </el-tabs>',
            '  ',
            '</div>'
        ].join(""),
        data: function () {
            return {
                activeName: 'third',
                port: {
                    HTTPSPort: '',
                    RTSPPort: '',
                    HTTPPort: '',
                    SDKPort: '',
                },
            }
        },
        created: function () {
            this.getSysPort();
        },
        methods: {
            tabClick: function () {
                var enumObj = {
                    first: '/admin/netConfig',
                    third: '/admin/netPort',
                    fifth: '/admin/netMobile',
                    sixth: '/admin/netWifi'
                }
                if(enumObj[this.activeName] && enumObj[this.activeName] !== this.$route.path) {
                    this.$router.push({
                        path: enumObj[this.activeName],
                        query: {
                            num: this.$route.query.num
                        }
                    })
                }
                return
            },
            getSysPort: function () {
                var _this = this;
                getSysPort({}).then(function (res) {
                    var data = res.data;
                    if (data.code == 1000) {
                        _this.port = data.data;
                    } else {
                        _this.$message.error("端口信息获取失败")
                    }
                }).catch(function (res) {
                    _this.$message.error("获取端口信息请求失败")
                    return;
                })
            },
            savePort: function () {
                var _this = this;
                setSysPort(_this.port).then(function (res) {
                    var data = res.data;
                    if (data.code == 1000) {
                        _this.$message.success("保存成功")
                    } else {
                        _this.$message.error("保存失败")
                    }
                }).catch(function (res) {
                    _this.$message.error("请求失败")
                    return;
                })
            },
        }
    })
})
