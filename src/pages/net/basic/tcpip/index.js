define(function (require, exports, module) {
    require('./index.css');

    var getNetwork = require('../../../../api/network.js').getNetwork;
    var setNetwork = require('../../../../api/network.js').setNetwork;

    module.exports = Vue.component('index', {
        template: ['<div>',
            '  <el-tabs v-model="activeName" @tab-click="tabClick">',
            '    <el-tab-pane label="TCP/IP" name="first">',
            '      <div class="net-item-box">',
            '        <label for="">{{ $t("network.gatewayType") }}</label>',
            '        <el-select class="net-input" size="mini" v-model="TCPIP.Type">',
            '           <el-option :label="$t(\'network.auto\')" :value="0"></el-option>',
            '           <el-option :label="$t(\'network.halfDuplex10\')" :value="1"></el-option>',
            '           <el-option :label="$t(\'network.fullDuplex10\')" :value="2"></el-option>',
            '           <el-option :label="$t(\'network.halfDuplex100\')" :value="3"></el-option>',
            '           <el-option :label="$t(\'network.fullDuplex100\')" :value="4"></el-option>',
            '           <el-option :label="$t(\'network.halfDuplex1000\')" :value="5"></el-option>',
            '           <el-option :label="$t(\'network.fullDuplex1000\')" :value="6"></el-option>',
            '        </el-select>',
            '      </div>',
            '      <div class="net-item-box">',
            '        <label for="">{{ $t("network.mode") }}</label>',
            '        <el-select class="net-input" size="mini" v-model="TCPIP.IPvxModel">',
            '           <el-option label="IPV4" :value="0"></el-option>',
            '           <el-option label="IPV6" :value="1"></el-option>',
            '        </el-select>',
            '      </div>',
            '      <div class="net-item-box">',
            '        <label for="">&nbsp;</label>',
            '        <el-checkbox class="net-input" style="text-align: left" :true-label="1" :false-label="0" v-model="TCPIP.DHCP">{{ $t("network.DHCP") }}</el-checkbox>',
            '      </div>',
            '      <span v-show="TCPIP.IPvxModel === 0 ">',
            '        <div class="net-item-box">',
            '          <label for="">{{ $t("network.IPv4Address") }}</label>',
            '          <el-input :disabled="disabledDHCP" style="width: 390px" class="net-input" size="mini" v-model="TCPIP.IPv4Address"></el-input>',
            '          <el-button type="primary" size="mini">{{ $t("network.test") }}</el-button>',
            '        </div>',
            '        <div class="net-item-box">',
            '          <label for="">{{ $t("network.IPv4Mask") }}</label>',
            '          <el-input :disabled="disabledDHCP" class="net-input" size="mini" v-model="TCPIP.IPv4Mask"></el-input>',
            '        </div>',
            '        <div class="net-item-box">',
            '          <label for="">{{ $t("network.IPv4Gateway") }}</label>',
            '          <el-input :disabled="disabledDHCP" class="net-input" size="mini" v-model="TCPIP.IPv4Gateway"></el-input>',
            '        </div>',
            '      </span>',
            '      <span v-show="TCPIP.IPvxModel === 1 ">',
            '        <div class="net-item-box">',
            '          <label for="">{{ $t("network.IPv6Address") }}</label>',
            '          <el-input :disabled="disabledDHCP" class="net-input" size="mini" v-model="TCPIP.IPv6Address"></el-input>',
            '        </div>',
            '        <div class="net-item-box">',
            '          <label for="">{{ $t("network.IPv6Mask") }}</label>',
            '          <el-input :disabled="disabledDHCP" class="net-input" size="mini" v-model="TCPIP.IPv6Mask"></el-input>',
            '        </div>',
            '        <div class="net-item-box">',
            '          <label for="">{{ $t("network.IPv6Gateway") }}</label>',
            '          <el-input :disabled="disabledDHCP" class="net-input" size="mini" v-model="TCPIP.IPv6Gateway"></el-input>',
            '        </div>',
            '      </span>',
            '      <div class="net-item-box">',
            '        <label for="">{{ $t("network.MacAddress") }}</label>',
            '        <el-input :disabled="disabledDHCP" class="net-input" size="mini" v-model="TCPIP.MacAddress"></el-input>',
            '      </div>',
            '      <div class="net-item-box">',
            '        <label for="">MTU</label>',
            '        <el-input :disabled="disabledDHCP" class="net-input" size="mini" v-model="TCPIP.MTU"></el-input>',
            '      </div>',
            '      <div class="net-item-box">',
            '        <label for="">{{ $t("network.Bcast") }}</label>',
            '        <el-input :disabled="disabledDHCP" class="net-input" size="mini" v-model="TCPIP.Bcast"></el-input>',
            '      </div>',
            '      <p style="margin: 10px auto 0; width: 570px;" class="p-title">{{ $t("network.DNS") }}</p>',
            '      <div class="net-item-box">',
            '        <label for="">{{ $t("network.firstDNS") }}</label>',
            '        <el-input :disabled="disabledDHCP" v-show="TCPIP.IPvxModel === 0 " class="net-input" size="mini" v-model="TCPIP.IPv4DNS"></el-input>',
            '        <el-input :disabled="disabledDHCP" v-show="TCPIP.IPvxModel === 1 " class="net-input" size="mini" v-model="TCPIP.IPv6DNS"></el-input>',
            '      </div>',
            '      <div class="net-item-box">',
            '        <label for="">{{ $t("network.secondDNS") }}</label>',
            '        <el-input :disabled="disabledDHCP" v-show="TCPIP.IPvxModel === 0 " class="net-input" size="mini" v-model="TCPIP.IPv4ReDNS"></el-input>',
            '        <el-input :disabled="disabledDHCP" v-show="TCPIP.IPvxModel === 1 " class="net-input" size="mini" v-model="TCPIP.IPv6ReDNS"></el-input>',
            '      </div>',
            '      <div style="text-align: center; margin-top: 10px;">',
            '        <el-button @click="saveTCPIP" icon="el-icon-receiving" size="mini" type="success">{{ $t("common.save") }}</el-button>',
            '      </div>',
            '    </el-tab-pane>',
            '    <el-tab-pane :label="$t(\'network.port\')" name="third">PORT</el-tab-pane>',
            '    <el-tab-pane :label="$t(\'network.mobile\')" name="fifth">MOBILE</el-tab-pane>',
            '    <el-tab-pane :label="$t(\'network.wificonfig\')" name="sixth">WIFI</el-tab-pane>',
            '  </el-tabs>',
            '  ',
            '</div>'
        ].join(""),
        data: function () {
            return {
                activeName: 'first',
                TCPIP: {
                    Type: 0,
                    IPvxModel: 0,
                    DHCP: 0,
                    IPv4Address: '',
                    IPv4Mask: '',
                    IPv4Gateway: '',
                    // IPv6Mode: '0',
                    IPv6Address: '',
                    IPv6Mask: '',
                    IPv6Gateway: '',
                    MacAddress: '',
                    MTU: '',
                    Bcast: '',
                    IPv4DNS: '',
                    IPv4ReDNS: '',
                    IPv6DNS: '',
                    IPv6ReDNS: ''
                },
            }
        },
        computed: {
            disabledDHCP: function () {
                return this.TCPIP.DHCP === 1
            }
        },
        created: function () {
            this.getNetwork();
        },
        methods: {
            tabClick: function () {
                var enumObj = {
                    first: '/admin/netConfig',
                    third: '/admin/netPort',
                    fifth: '/admin/netMobile',
                    sixth: '/admin/netWifi'
                }
                if(enumObj[this.activeName] && enumObj[this.activeName] !== this.$route.path) {
                    this.$router.push({
                        path: enumObj[this.activeName],
                        query: {
                            num: this.$route.query.num
                        }
                    })
                }
                return
            },
            getNetwork: function () {
                var _this = this;
                getNetwork({}).then(function (res) {
                    var data = res.data;
                    if (data.code == 1000) {
                        _this.TCPIP = data.data;
                    } else {
                        _this.$message.error("网络信息获取失败")
                    }
                }).catch(function (res) {
                    _this.$message.error("获取网络信息请求失败")
                    return;
                })
            },
            saveTCPIP: function () {
                var _this = this;
                var checks = (_this.TCPIP.IPvxModel === 0) ? {
                    IPv4Address: 'IPv4地址',
                    IPv4Mask: 'IPv4子网掩码',
                    IPv4Gateway: 'IPv4默认网关',
                    IPv4DNS: 'IPv4 首选DNS服务器',
                    IPv4ReDNS: 'IPv4 备用DNS服务器'
                } : {
                        IPv6DNS: 'IPv6 首选DNS服务器',
                        IPv6ReDNS: 'IPv6 备用DNS服务器'
                    }
                for (var key in checks) {
                    if (!_this.checkIP(_this.TCPIP[key])) {
                        _this.$message.error(checks[key] + "输入地址不合法！")
                        return
                    }
                }

                setNetwork(_this.TCPIP).then(function (res) {
                    var data = res.data;
                    if (data.code == 1000) {
                        _this.$message.success("保存成功")
                    } else {
                        _this.$message.error("保存失败")
                    }
                }).catch(function (res) {
                    _this.$message.error("请求失败")
                    return;
                })
            },
            checkIP: function (val) {
                var re = /^(\d+)\.(\d+)\.(\d+)\.(\d+)$/; //正则表达式
                if (re.test(val)) {
                    if (RegExp.$1 < 256 && RegExp.$2 < 256 && RegExp.$3 < 256 && RegExp.$4 < 256)
                        return true;
                }
                return false;
            },
        }
    })
})
