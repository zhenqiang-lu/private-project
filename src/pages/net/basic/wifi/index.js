define(function (require, exports, module) {
    require('./index.css');

    var getWifiConfig = require('../../../../api/network.js').getWifiConfig;
    var setWifiConfig = require('../../../../api/network.js').setWifiConfig;

    module.exports = Vue.component('index', {
        template: ['<div>',
            '  <el-tabs v-model="activeName" @tab-click="tabClick">',
            '    <el-tab-pane label="TCP/IP" name="first">TCPIP</el-tab-pane>',
            '    <el-tab-pane :label="$t(\'network.port\')" name="third">PORT</el-tab-pane>',
            '    <el-tab-pane :label="$t(\'network.mobile\')" name="fifth">MOBILE</el-tab-pane>',
            '    <el-tab-pane :label="$t(\'network.wificonfig\')" name="sixth">',
            '          <div class="net-item-box">',
            '            <label for="">{{ $t(\'common.enable\') }}</label>',
            '            <el-checkbox :true-label="1" :false-label="0" style="width: 450px; text-align: left" v-model="WIFIconfig.Enable"></el-checkbox>',
            '          </div>',
            '          <div class="net-item-box">',
            '            <label for="">{{ $t(\'network.WifiMode\') }}</label>',
            '            <el-select :disabled=" WIFIconfig.Enable === 0 " class="net-input" size="mini" v-model="WIFIconfig.Mode">',
            '              <el-option label="STA" :value="0"></el-option>',
            '              <el-option label="AP" :value="1"></el-option>',
            '            </el-select>',
            '          </div>',
            '          <div class="net-item-box">',
            '            <label for="">{{ $t(\'network.WifiType\') }}</label>',
            '            <el-select :disabled=" WIFIconfig.Enable === 0 " class="net-input" size="mini" v-model="WIFIconfig.Type">',
            '              <el-option label="2.4G" :value="0"></el-option>',
            '              <el-option label="5G" :value="1"></el-option>',
            '              <el-option label="6G" :value="2"></el-option>',
            '            </el-select>',
            '          </div>',
            '          <div class="net-item-box">',
            '            <label for="">SSID</label>',
            '            <el-input :disabled=" WIFIconfig.Enable === 0 " class="net-input" size="mini" v-model="WIFIconfig.SSID"></el-input>',
            '          </div>',
            '          <div v-show="WIFIconfig.Mode === 1" class="net-item-box">',
            '            <label for="">{{ $t(\'network.EncryptionMode\') }}</label>',
            '            <el-select :disabled=" WIFIconfig.Enable === 0 " class="net-input" size="mini" v-model="WIFIconfig.EncryMode">',
            '              <el-option :label="$t(\'common.auto\')" :value="0"></el-option>',
            '              <el-option label="WPA-PSK/WPA2-PSK" :value="1"></el-option>',
            '              <el-option label="WPA/WPA2" :value="2"></el-option>',
            '            </el-select>',
            '          </div>',
            '          <div class="net-item-box">',
            '            <label for="">{{ $t(\'network.AuthType\') }}</label>',
            '            <el-select :disabled=" WIFIconfig.Enable === 0 " class="net-input" size="mini" v-model="WIFIconfig.AuthType">',
            '              <el-option :label="$t(\'common.auto\')" :value="0"></el-option>',
            '              <el-option label="WPA-PSK" :value="1"></el-option>',
            '              <el-option label="WPA2-PSK" :value="2"></el-option>',
            '            </el-select>',
            '          </div>',
            '          <div v-show="WIFIconfig.Mode === 1" class="net-item-box">',
            '            <label for="">{{ $t(\'network.EncryptionAlg\') }}</label>',
            '            <el-select :disabled=" WIFIconfig.Enable === 0 " class="net-input" size="mini" v-model="WIFIconfig.EncryAlg">',
            '              <el-option :label="$t(\'common.auto\')" :value="0"></el-option>',
            '              <el-option label="TKIP" :value="1"></el-option>',
            '              <el-option label="AES" :value="2"></el-option>',
            '            </el-select>',
            '          </div>',
            '          <div class="net-item-box">',
            '            <label for="">{{ $t(\'message.password\') }}</label>',
            '            <el-input :disabled=" WIFIconfig.Enable === 0 " :type="viewPwd ? \'text\' : \'password\' " class="net-input" size="mini" v-model="WIFIconfig.Password">',
            '              <i @click=" viewPwd = !viewPwd " class="el-icon-view el-input__icon" slot="suffix"></i>',
            '            </el-input>',
            '          </div>',
            '          <div v-show="WIFIconfig.Mode === 1" class="net-item-box">',
            '            <label for="">{{ $t(\'network.channel\') }}</label>',
            '            <el-select :disabled=" WIFIconfig.Enable === 0 " class="net-input" size="mini" v-model="WIFIconfig.SigChn">',
            '              <el-option :label="$t(\'common.auto\')" :value="0"></el-option>',
            '              <el-option v-for="(item, index) in channels" :key="index" :label="item" :value="item"></el-option>',
            '            </el-select>',
            '          </div>',
            '          <div v-show="WIFIconfig.Mode === 0" class="net-item-box">',
            '            <label for="">{{ $t("network.IPDZ") }}</label>',
            '            <el-input :disabled=" WIFIconfig.DHCP === 0 || WIFIconfig.Enable === 0 " class="net-input" style="width: 316px" size="mini" v-model="WIFIconfig.IPAddr"></el-input>',
            '            <el-checkbox style="margin-left: 10px" :true-label="0" :false-label="1" v-model="WIFIconfig.DHCP">{{ $t("network.DHCP") }}</el-checkbox>',
            '          </div>',
            '          <div v-show="WIFIconfig.Mode === 0" class="net-item-box">',
            '            <label for="">{{ $t("network.ZWYM") }}</label>',
            '            <el-input :disabled=" WIFIconfig.DHCP === 0 || WIFIconfig.Enable === 0 " class="net-input" size="mini" v-model="WIFIconfig.Mask"></el-input>',
            '          </div>',
            '          <div v-show="WIFIconfig.Mode === 0" class="net-item-box">',
            '            <label for="">{{ $t("network.MRWG") }}</label>',
            '            <el-input :disabled=" WIFIconfig.DHCP === 0 || WIFIconfig.Enable === 0 " class="net-input" size="mini" v-model="WIFIconfig.Gateway"></el-input>',
            '          </div>',
            '          <div v-show="WIFIconfig.Mode === 0" class="net-item-box">',
            '            <label for="">{{ $t("network.DNSK") }}</label>',
            '            <el-input :disabled=" WIFIconfig.DHCP === 0 || WIFIconfig.Enable === 0 " class="net-input" size="mini" v-model="WIFIconfig.DNS"></el-input>',
            '          </div>',
            '          <div style="text-align: center; margin-top: 10px;">',
            '            <el-button @click="setWifiConfig" icon="el-icon-receiving" size="mini" type="success">{{ $t(\'common.save\') }}</el-button>',
            '          </div>',
            '    </el-tab-pane>',
            '  </el-tabs>',
            '  ',
            '</div>'
        ].join(""),
        data: function () {
            return {
                activeName: 'sixth',
                viewPwd: false,
                WIFIconfig: {
                    Enable: 1,
                    Mode: 0,
                    Type: 0,
                    SSID: "WIFI_5G",
                    EncryMode: 0,
                    AuthType: 0,
                    EncryAlg: 0,
                    Password: "admin",
                    SigChn: 0,
                    DHCP: 0,
                    IPAddr: '',
                    Mask: '',
                    Gateway: '',
                    DNS: ''
                },
            }
        },

        created: function () {
            this.getWifiConfig()
        },

        computed: {
            channels: function() {
                var channel_2_4 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
                var channel_5 = [36, 40, 44, 48, 149, 153, 157, 161, 165]
                return (
                    this.WIFIconfig.Type === 0 ?
                    channel_2_4 :
                    channel_5
                )
            }
        },

        methods: {
            tabClick: function () {
                var enumObj = {
                    first: '/admin/netConfig',
                    third: '/admin/netPort',
                    fifth: '/admin/netMobile',
                    sixth: '/admin/netWifi'
                }
                if (enumObj[this.activeName] && enumObj[this.activeName] !== this.$route.path) {
                    this.$router.push({
                        path: enumObj[this.activeName],
                        query: {
                            num: this.$route.query.num
                        }
                    })
                }
                return
            },
            getWifiConfig: function () {
                var _this = this;
                getWifiConfig({}).then(function (res) {
                    var data = res.data;
                    if (data.code == 1000) {
                        _this.WIFIconfig = data.data;
                    } else {
                        _this.$message.error("wifi配置获取失败")
                    }
                }).catch(function (res) {
                    _this.$message.error("wifi配置请求失败")
                    return;
                })
            },
            setWifiConfig: function () {
                var _this = this;
                setWifiConfig(_this.WIFIconfig).then(function (res) {
                    var data = res.data;
                    if (data.code == 1000) {
                        _this.$message.success("保存成功");
                        _this.getWifiConfig();
                    } else {
                        _this.$message.error("保存失败")
                    }
                }).catch(function (res) {
                    _this.$message.error("请求失败")
                    return;
                })
            },
        }
    })
})
