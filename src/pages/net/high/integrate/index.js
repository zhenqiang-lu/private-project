define(function(require, exports, module) {
    require('./index.css');

    var getONVIFServer = require('../../../../api/network.js').getONVIFServer;
    var setONVIFServer = require('../../../../api/network.js').setONVIFServer;

    module.exports = Vue.component('index', {
        template: ['<div>',
            '  <el-tabs v-model="activeName" @tab-click="tabClick">',
            '    <el-tab-pane :label="$t(\'network.GB28281Devcie\')" name="fourth">SNMP</el-tab-pane>',
            '    <el-tab-pane :label="$t(\'network.ONVIFServer\')" name="eighth">',
            '      <div class="net-item-box">',
            '        <label for="">{{ $t(\'common.enable\') }}</label>',
            '        <el-checkbox v-model="integrate.Enable" :true-label="1" :false-label="0" style="width: 450px; text-align: left;"></el-checkbox>',
            '      </div>',
            '      <div class="net-item-box">',
            '        <label for="">HTTPS</label>',
            '        <el-select :disabled=" integrate.Enable === 0 " class="net-input" size="mini" v-model="integrate.Https">',
            '           <el-option :label="$t(\'common.disable\')" :value="0"></el-option>',
            '           <el-option :label="$t(\'common.enable\')" :value="1"></el-option>',
            '        </el-select>',
            '      </div>',
            '      <div class="net-item-box">',
            '        <label for="">{{ $t(\'network.Version\') }}</label>',
            '        <el-input disabled class="net-input" size="mini" v-model="integrate.Version"></el-input>',
            '      </div>',
            '      <div class="net-item-box">',
            '        <label for="">{{ $t(\'network.Port\') }}</label>',
            '        <el-input :disabled=" integrate.Enable === 0 " class="net-input" size="mini" v-model="integrate.Port"></el-input>',
            '      </div>',
            '      <div class="net-item-box">',
            '        <label for="">{{ $t(\'network.Auth\') }}</label>',
            '        <el-select :disabled=" integrate.Enable === 0 " class="net-input" size="mini" v-model="integrate.Auth">',
            '           <el-option label="none" :value="0"></el-option>',
            '           <el-option label="WS-username token" :value="1"></el-option>',
            '           <el-option label="Digest" :value="2"></el-option>',
            '        </el-select>',
            '      </div>',
            '      <div class="net-item-box">',
            '        <label for="">{{ $t(\'network.MaxUsers\') }}</label>',
            '        <el-input :disabled=" integrate.Enable === 0 " class="net-input" size="mini" v-model="integrate.MaxUsers"></el-input>',
            '      </div>',
            '        <div style="text-align: center; margin-top: 10px;">',
            '          <el-button @click="setONVIFServer" icon="el-icon-receiving" size="mini" type="success">{{ $t(\'common.save\') }}</el-button>',
            '        </div>',
            '    </el-tab-pane>',
            '    ',
            '  </el-tabs>',
            '  ',
            '</div>'
        ].join(""),
        data: function() { // 数据
            return {
                addUserDialog: false,
                activeName: 'eighth',
                integrate: {
                    Enable: 1, //  启用    0禁止，1启用
                    Version: '',   // 协议版本 字符串
                    Port: 2, // 端口 整型
                    Auth: 0,    // 认证 0禁止，1启用
                    Https: 1,   // HTTPS 0禁止，1启用
                    MaxUsers: 0 // 最大用户  整型

                }
            }
        },
        created: function () {
            this.getONVIFServer();
        },
        methods: {
            getONVIFServer: function() {
                var _this = this;
                getONVIFServer({}).then(function (res) {
                    var data = res.data;
                    if (data.code == 1000) {
                        _this.integrate = data.data;
                    } else {
                        _this.$message.error("ONVIF Server获取失败")
                    }
                }).catch(function (res) {
                    _this.$message.error("获取ONVIF Server请求失败")
                    return;
                })
            },
            setONVIFServer: function () {
                var _this = this;
                var parseIntList = ['MaxUsers',  'Port'];
                for(var i = 0 ; i < parseIntList.length; i ++) {
                    var tp = parseIntList[i]
                    _this.integrate[tp] = _this.integrate[tp] * 1
                }
                setONVIFServer(_this.integrate).then(function (res) {
                    var data = res.data;
                    if (data.code == 1000) {
                        _this.$message.success("保存成功");
                        _this.getONVIFServer();
                    } else {
                        _this.$message.error("保存失败")
                    }
                }).catch(function (res) {
                    _this.$message.error("请求失败")
                    return;
                })
            },
            tabClick: function () {
                var enumObj = {
                    fourth: '/admin/netHighConfig',
                    eighth: '/admin/integrate'
                }
                if(enumObj[this.activeName] && enumObj[this.activeName] !== this.$route.path) {
                    this.$router.push({
                        path: enumObj[this.activeName],
                        query: {
                            num: this.$route.query.num
                        }
                    })
                }
                return
            },
        }
    })
})
