define(function(require, exports, module) {
    require('./index.css');

    var getGB28281Device = require('../../../../api/network.js').getGB28281Device;
    var setGB28281Device = require('../../../../api/network.js').setGB28281Device;

    module.exports = Vue.component('index', {
        template: ['<div>',
            '  <el-tabs v-model="activeName" @tab-click="tabClick">',
            '    <el-tab-pane name="fourth" :label="$t(\'network.GB28281Devcie\')">',
            '      <div>',
            '        <div class="net-item-box">',
            '          <label for="">{{ $t(\'common.enable\') }}</label>',
            '          <el-checkbox v-model="platform.Enable" :true-label="1" :false-label="0" style="width: 450px; text-align: left;"></el-checkbox>',
            '        </div>',
            '        <div class="net-item-box">',
            '          <label for="">{{ $t(\'network.Version\') }}</label>',
            '          <el-select :disabled=" platform.Enable === 0 " class="net-input" size="mini" v-model="platform.Version">',
            '             <el-option label="GB28281-2016" :value="0"></el-option>',
            '          </el-select>',
            '        </div>',
            '        <div class="net-item-box">',
            '          <label for="">{{ $t(\'network.ServerIP\') }}</label>',
            '          <el-input :disabled=" platform.Enable === 0 " class="net-input" size="mini" v-model="platform.ServerIP"></el-input>',
            '        </div>',
            '        <div class="net-item-box">',
            '          <label for="">{{ $t(\'network.ServerPort\') }}</label>',
            '          <el-input :disabled=" platform.Enable === 0 " class="net-input" size="mini" v-model="platform.ServerPort"></el-input>',
            '        </div>',
            '        <div class="net-item-box">',
            '          <label for="">{{ $t(\'network.ServerID\') }}</label>',
            '          <el-input :disabled=" platform.Enable === 0 " class="net-input" size="mini" v-model="platform.ServerID"></el-input>',
            '        </div>',
            '        <div class="net-item-box">',
            '          <label for="">{{ $t(\'network.ServerDomain\') }}</label>',
            '          <el-input :disabled=" platform.Enable === 0 " class="net-input" size="mini" v-model="platform.ServerDomain"></el-input>',
            '        </div>',
           
            '        <div class="net-item-box">',
            '          <label for="">{{ $t(\'network.DeviceID\') }}</label>',
            '          <el-input :disabled=" platform.Enable === 0 " class="net-input" size="mini" v-model="platform.DeviceID"></el-input>',
            '        </div>',
            '        <div class="net-item-box">',
            '          <label for="">{{ $t(\'network.ChannelID\') }}</label>',
            '          <el-input :disabled=" platform.Enable === 0 " class="net-input" size="mini" v-model="platform.ChannelID"></el-input>',
            '        </div>',
            '        <div class="net-item-box">',
            '          <label for="">{{ $t(\'user.pwd\') }}</label>',
            '          <el-input :disabled=" platform.Enable === 0 " :type="viewPwd ? \'text\' : \'password\' " class="net-input" size="mini" v-model="platform.Password">',
            '              <i @click=" viewPwd = !viewPwd " class="el-icon-view el-input__icon" slot="suffix"></i>',
            '          </el-input>',
            '        </div>',
            '        <div class="net-item-box">',
            '          <label for="">{{ $t(\'user.surePwd\') }}</label>',
            '          <el-input :disabled=" platform.Enable === 0 " :type="viewRePwd ? \'text\' : \'password\' " class="net-input" size="mini" v-model="rePwd">',
            '              <i @click=" viewRePwd = !viewRePwd " class="el-icon-view el-input__icon" slot="suffix"></i>',
            '          </el-input>',
            '        </div>',
            '        <div class="net-item-box">',
            '          <label for="">{{ $t(\'network.RegExpires\') }}</label>',
            '          <el-input :disabled=" platform.Enable === 0 " class="net-input" size="mini" v-model.num="platform.RegExpires"></el-input>',
            '        </div>',
            '        <div class="net-item-box">',
            '          <label for="">{{ $t(\'network.HbInterval\') }}</label>',
            '          <el-input :disabled=" platform.Enable === 0 " class="net-input" size="mini" v-model.num="platform.HBInterval"></el-input>',
            '        </div>',
            '        <div class="net-item-box">',
            '          <label for="">{{ $t(\'network.HbCount\') }}</label>',
            '          <el-input :disabled=" platform.Enable === 0 " class="net-input" size="mini" v-model.num="platform.HBCount"></el-input>',
            '        </div>',
            '        <div class="net-item-box">',
            '          <label for="">{{ $t(\'network.Protocol\') }}</label>',
            '          <el-select :disabled=" platform.Enable === 0 " class="net-input" size="mini" v-model.num="platform.Protocol">',
            '             <el-option label="TCP" :value="0"></el-option>',
            '             <el-option label="UDP" :value="1"></el-option>',
            '          </el-select>',
            '        </div>',
            '        <div style="text-align: center; margin-top: 10px;">',
            '          <el-button @click="setGB28281Device" icon="el-icon-receiving" size="mini" type="success">{{ $t(\'common.save\') }}</el-button>',
            '        </div>',
            '      </div>',
            '    </el-tab-pane>',
            '    <el-tab-pane :label="$t(\'network.ONVIFServer\')" name="eighth">SNMP</el-tab-pane>',
            '  </el-tabs>',
            '</div>'
        ].join(""),
        data: function() { // 数据
            return {
                activeName: 'fourth',
                rePwd: '',
                viewPwd: false,
                viewRePwd: false,
                platform: {
                    Enable: 1,  //启用
                    Version: 0,  //协议版本
                    ServerIP: '',  //SIP服务器IP
                    ServerPort: '', //SIP服务器端口
                    ServerID: '',  //SIP服务器ID
                    ServerDomain: '',  //SIP服务器域
                    DeviceID: '', //设备ID
                    ChannelID: '', //通道ID
                    Password: '', // 密码
                    Protocol: 0,   // 传输协议
                    RegExpires: 0,  //注册有效期
                    HBInterval:0,   //心跳周期
                    HBCount: 0,   //心跳超时次数
                },
            }
        },
        created: function () {
            this.getGB28281Device();
        },
        methods: {
            getGB28281Device: function() {
                var _this = this;
                getGB28281Device({}).then(function (res) {
                    var data = res.data;
                    if (data.code == 1000) {
                        _this.platform = data.data;
                        _this.rePwd = _this.platform.Password + ''
                    } else {
                        _this.$message.error("GB28281 Device获取失败")
                    }
                }).catch(function (res) {
                    _this.$message.error("获取GB28281 Device请求失败")
                    return;
                })
            },
            setGB28281Device: function () {
                var _this = this;
                if(this.rePwd !== this.platform.Password) {
                    this.$message.error("两次密码不一致");
                    return;
                }
                var parseIntList = ['RegExpires',  'HBInterval', 'HBCount'];
                for(var i = 0 ; i < parseIntList.length; i ++) {
                    var tp = parseIntList[i]
                    _this.platform[tp] = _this.platform[tp] * 1
                }
                setGB28281Device(_this.platform).then(function (res) {
                    var data = res.data;
                    if (data.code == 1000) {
                        _this.$message.success("保存成功");
                        _this.getGB28281Device();
                    } else {
                        _this.$message.error("保存失败")
                    }
                }).catch(function (res) {
                    _this.$message.error("请求失败")
                    return;
                })
            },
            tabClick: function () {
                var enumObj = {
                    fourth: '/admin/netHighConfig',
                    eighth: '/admin/integrate'
                }
                if(enumObj[this.activeName] && enumObj[this.activeName] !== this.$route.path) {
                    this.$router.push({
                        path: enumObj[this.activeName],
                        query: {
                            num: this.$route.query.num
                        }
                    })
                }
                return
            },
        }
    })
})
