define(function(require, exports, module) {
    require('./index.css');

    var getPicParam = require('../../../../api/pic.js').getPicParam;
    var setPicParam = require('../../../../api/pic.js').setPicParam;

    module.exports = Vue.component('index', {
        template: ['<div style="margin-bottom: 50px">',
            '  <el-tabs v-model="activeName">',
            '    <el-tab-pane label="图像质量" name="first">',
            '      <div class="left-box">',
            '        <div id="dplayer" style="width: 100% margin: 0 auto;">',
            '          <video id="videoElement" autoplay controls width="100%"></video>',
            '        </div>',
            '        <h4 class="sec-title-pic">{{ $t(\'pic.dayAndNight\') }}</h4>',
            '        <div class="pic-item-box">',
            '          <label for="">{{ $t(\'pic.mode\') }}</label>',
            '          <el-select v-model="pic.Mode" class="input-box" size="mini">',
            '            <el-option :label="$t(\'pic.auto2\')" :value="0"></el-option>',
            '            <el-option :label="$t(\'pic.day\')" :value="1"></el-option>',
            '            <el-option :label="$t(\'pic.night\')" :value="2"></el-option>',
            '          </el-select>',
            '        </div>',
            '        <div class="pic-item-box">',
            '          <label for="">{{ $t(\'pic.sensitivity\') }}</label>',
            '          <el-select v-model="pic.Sensitivity" class="input-box" size="mini">',
            '            <el-option :label="$t(\'pic.low\')" :value="0"></el-option>',
            '            <el-option :label="$t(\'pic.middle\')" :value="1"></el-option>',
            '            <el-option :label="$t(\'pic.high\')" :value="2"></el-option>',
            '          </el-select>',
            '        </div>',
            '        <div class="pic-item-box">',
            '          <label for="">{{ $t(\'pic.timeSwitching\') }}</label>',
            '          <el-slider v-model="pic.Time" :max="120" class="slider-box-div" :show-input-controls="false" input-size="mini" show-input></el-slider>',
            '        </div>',
            '        <div class="pic-item-box">',
            '          <label for="">{{ $t(\'pic.fillLight\') }}</label>',
            '          <el-select v-model="pic.FillLight" class="input-box" size="mini">',
            '            <el-option :label="$t(\'pic.close\')" :value="0"></el-option>',
            '            <el-option :label="$t(\'pic.open\')" :value="1"></el-option>',
            '          </el-select>',
            '        </div>',
            '      </div>',
            '      <div style="height: 800px" class="middle-box"></div>',
            '      <div class="right-box">',
            '        <h4 class="sec-title-pic">{{ $t(\'pic.imageEnhancement\') }}</h4>',
            '        <div class="pic-item-box">',
            '          <label for="">{{ $t(\'pic.light\') }}</label>',
            '          <el-slider v-model="pic.Brightness" class="slider-box-div" :show-input-controls="false" input-size="mini" show-input></el-slider>',
            '        </div>',
            '        <div class="pic-item-box">',
            '          <label for="">{{ $t(\'pic.saturation\') }}</label>',
            '          <el-slider v-model="pic.Saturation" class="slider-box-div" :show-input-controls="false" input-size="mini" show-input></el-slider>',
            '        </div>',
            '        <div class="pic-item-box">',
            '          <label for="">{{ $t(\'pic.contrast\') }}</label>',
            '          <el-slider v-model="pic.Contrast" class="slider-box-div" :show-input-controls="false" input-size="mini" show-input></el-slider>',
            '        </div>',
            '        <div class="pic-item-box">',
            '          <label for="">{{ $t(\'pic.sharpness\') }}</label>',
            '          <el-slider v-model="pic.sharpness" class="slider-box-div" :show-input-controls="false" input-size="mini" show-input></el-slider>',
            '        </div>',
            '        <div class="pic-item-box">',
            '          <label for="">{{ $t(\'pic._2D\') }}</label>',
            '          <el-slider v-model="pic.SoiseReduction2D" class="slider-box-div" :show-input-controls="false" input-size="mini" show-input></el-slider>',
            '        </div>',
            '        <div class="pic-item-box">',
            '          <label for="">{{ $t(\'pic._3D\') }}</label>',
            '          <el-slider v-model="pic.NoiseReduction3D" class="slider-box-div" :show-input-controls="false" input-size="mini" show-input></el-slider>',
            '        </div>',
            '        <div class="pic-item-box">',
            '          <label for="">{{ $t(\'pic.image\') }}</label>',
            '          <el-select v-model="pic.Mirror" class="input-box" size="mini">',
            '            <el-option :label="$t(\'pic.normal\')" :value="0"></el-option>',
            '            <el-option :label="$t(\'pic.right90\')" :value="1"></el-option>',
            '            <el-option :label="$t(\'pic.right180\')" :value="2"></el-option>',
            '            <el-option :label="$t(\'pic.right270\')" :value="3"></el-option>',
            '          </el-select>',
            '        </div>',
            '        <h4 class="sec-title-pic">{{ $t(\'pic.focusingParameters\') }}</h4>',
            '        <div class="pic-item-box">',
            '          <label for="">{{ $t(\'pic.focusingMode\') }}</label>',
            '          <el-select v-model="pic.AfMode" class="input-box" size="mini">',
            '            <el-option :label="$t(\'pic.auto\')" :value="0"></el-option>',
            '            <el-option :label="$t(\'pic.hand\')" :value="1"></el-option>',
            '            <el-option :label="$t(\'pic.red\')" :value="2"></el-option>',
            '          </el-select>',
            '        </div>',
            '        <div class="pic-item-box">',
            '          <label for="">{{ $t(\'pic.focusingScene\') }}</label>',
            '          <el-select v-model="pic.AfScene" class="input-box" size="mini">',
            '            <el-option :label="$t(\'pic.normal2\')" :value="0"></el-option>',
            '            <el-option :label="$t(\'pic.farthest\')" :value="1"></el-option>',
            '          </el-select>',
            '        </div>',
            '        <div class="pic-item-box">',
            '          <label for="">{{ $t(\'pic.changeSpeed\') }}</label>',
            '          <el-select v-model="pic.ChangeOfTimes" class="input-box" size="mini">',
            '            <el-option :label="$t(\'pic.lowSpeed\')" :value="0"></el-option>',
            '            <el-option :label="$t(\'pic.highSpeed\')" :value="1"></el-option>',
            '          </el-select>',
            '        </div>',
            '        <div class="pic-item-box">',
            '          <label for="">{{ $t(\'pic.fillLightMode\') }}</label>',
            '          <el-select v-model="pic.FillLightMode" class="input-box" size="mini">',
            '            <el-option :label="$t(\'pic.mixedFill\')" :value="0"></el-option>',
            '            <el-option :label="$t(\'pic.lightWhiteMode\')" :value="1"></el-option>',
            '            <el-option :label="$t(\'pic.infraredMode\')" :value="2"></el-option>',
            '            <el-option :label="$t(\'pic.laserMode\')" :value="3"></el-option>',
            '          </el-select>',
            '        </div>',
            '        <div class="pic-item-box">',
            '          <label for="">{{ $t(\'pic.brightness\') }}</label>',
            '          <el-slider v-model="pic.BrightnessControl" class="slider-box-div" :show-input-controls="false" input-size="mini" show-input></el-slider>',
            '        </div>',
            '      <div style="width: 100%; text-align: center; float: left; margin-top: 10px;">',
            '        <el-button @click="setPicParam" icon="el-icon-receiving" size="mini" type="success">{{ $t(\'common.save\') }}</el-button>',
            '      </div>',
            '      </div>',
            '    </el-tab-pane>',
            '  </el-tabs>',
            '</div>',
        ].join(""),
        data: function() { // 数据
            return {
                activeName: 'first',
                pic: {
                    Brightness: 0,   //亮度
                    Saturation: 0,   //饱和度 
                    Contrast: 0,   //对比度
                    sharpness: 0,   //锐度
                    SoiseReduction2D: 0,   //2D降噪
                    NoiseReduction3D: 0,   //3D降噪
                    Mirror: '',   //图像镜像
                    AfMode: '',   //对焦模式
                    AfScene: '',   //对焦场景
                    ChangeOfTimes: '',   //变倍速度
                    Mode: '',   //模式
                    Sensitivity: '',   //灵敏度
                    Time: 0,   //切换时间
                    FillLight: '',   //补光
                    FillLightMode: '',   //补光模式
                    BrightnessControl: 0,  // 亮度调节
                }
            }
        },
        created: function () {
            this.getPicParam();
        },
        mounted: function () {
            if (flvjs.isSupported()) {
                var videoElement = document.getElementById('videoElement')
                this.flvPlayer = flvjs.createPlayer({
                    type: 'flv',
                    cors: true,
                    isLive: true,
                    url: 'http://192.168.31.117/myapp/yequ.mp4'
                }, {
                    stashInitialSize: '1200KB',
                    enableStashBuffer: false
                });
                this.flvPlayer.attachMediaElement(videoElement);
                this.flvPlayer.load()
                this.flvPlayer.play()
            }

        },
        methods: {
            play: function () {
                this.flvPlayer.play();
            },
            getPicParam: function () {
                var _this = this;
                getPicParam({}).then(function (res) {
                    var data = res.data;
                    if (data.code == 1000) {
                        _this.pic = data.data;
                    } else {
                        _this.$message.error("音频参数获取失败")
                    }
                }).catch(function (res) {
                    _this.$message.error("获取音频参数请求失败")
                    return;
                })
            },
            setPicParam: function () {
                var _this = this;
                setPicParam(_this.pic).then(function (res) {
                    var data = res.data;
                    if (data.code == 1000) {
                        _this.$message.success("保存成功");
                        _this.getPicParam();
                    } else {
                        _this.$message.error("保存失败")
                    }
                }).catch(function (res) {
                    _this.$message.error("请求失败")
                    return;
                })
            },
        }
    })
})
