define(function (require, exports, module) {
    require('./index.css');
    var toDate = require('../../../tools/tools.js').toDate;

    var usersList = require('../../../api/system.js').usersList;
    var usersDel = require('../../../api/system.js').usersDel;
    var usersAdd = require('../../../api/system.js').usersAdd;
    var usersUpdate = require('../../../api/system.js').usersUpdate;

    module.exports = Vue.component('index', {
        template: ['<div>',
            '  <el-tabs v-model="activeName">',
            '    <el-tab-pane :label="$t(\'user.userList\')" name="first">',
            '      <p style="margin-top: 10px;" class="p-title">',
            '        <span>{{$t(\'user.userList\')}}</span> ',
            '        <span class="right">',
            '          <el-button @click="addUser" class="user-btn" type="primary" size="mini">{{$t(\'user.add\')}}</el-button>',
            '          <el-button @click="updateUser" :disabled="!currentRow" type="success" class="user-btn" size="mini">{{$t(\'common.update\')}}</el-button>',
            '          <el-button @click="delUser" :disabled="deleteDisabled" type="danger" class="user-btn" size="mini">{{$t(\'common.del\')}}</el-button>',
            '        </span>',
            '      </p>',
            '      <el-table ref="singleTable" highlight-current-row @current-change="handleCurrentChange" border :data="allUsers.tableData" style="width: 100%; margin-top: 10px;">',
            '        <el-table-column align="center" width="90" :label="$t(\'common.number\')" type="index"></el-table-column>',
            '        <el-table-column align="center" prop="UserName" :label="$t(\'user.username\')"></el-table-column>',
            '        <el-table-column align="center" :label="$t(\'user.role\')">',
            '          <template slot-scope="scope">',
            '            <span>{{ scope.row.UserType | userTypeFilter }}</span>',
            '          </template>',
            '        </el-table-column>',
            '        <el-table-column align="center" :label="$t(\'user.onlineStatus\')">',
            '          <template slot-scope="scope">',
            '            <span :class=" scope.row.Online == 1 ? \'greenLabel\' : \'redLabel\' ">{{ scope.row.Online | onlineFilter }}</span>',
            '          </template>',
            '        </el-table-column>',
            '        <el-table-column align="center" prop="LoginTime" :label="$t(\'user.loginTime\')"></el-table-column>',
            '        <el-table-column align="center" prop="CreateTime" :label="$t(\'user.createTime\')"></el-table-column>',
            '      </el-table>',
            '    </el-tab-pane>',
            '  </el-tabs>',
            '  ',
            '  <el-dialog :title="$t(\'user.addUser\')" :close-on-click-modal="false" :visible.sync="dialogUser">',
            '    <div style="text-align: left; padding-left: 40px">',
            '      <div class="dialog-item-box">',
            '        <label for="">{{$t(\'user.username\')}}</label>',
            '        <el-input class="dialog-item-input" v-model="dialogData.UserName" size="mini"></el-input>',
            '        <p style="color: red; text-indent: 125px; padding-bottom: 8px;" v-show="legalUser">用户名6-18字符之间字母、字母+数字组合</p>',
            '      </div>',
            '      <div class="dialog-item-box">',
            '        <label for="">{{$t(\'user.role\')}}</label>',
            '        <el-select class="dialog-item-input" v-model="dialogData.UserType" placeholder="select" size="mini">',
            '          <el-option :label="$t(\'user.admin\')" :value="0"></el-option>',
            '          <el-option :label="$t(\'user.pt\')" :value="1"></el-option>',
            '          <el-option :label="$t(\'user.gz\')" :value="2"></el-option>',
            '        </el-select>',
            '      </div>',
            '      <div class="dialog-item-box">',
            '        <label for="">{{$t(\'user.pwd\')}}</label>',
            '        <el-input :type=" pwdview.pwd ? \'text\' : \'password\' " class="dialog-item-input" v-model="dialogData.Password" size="mini">',
            '          <i @click=" pwdview.pwd = !pwdview.pwd " class="el-icon-view el-input__icon" slot="suffix"></i>',
            '        </el-input>',
            '        <p style="color: red; text-indent: 125px; padding-bottom: 8px;" v-show="(level < 2)">密码6-18个字符之间字母、字母+数字、字母+数字+符号任意组合</p>',
            '      </div>',
            '      <div class="dialog-item-box">',
            '        <label for=""></label></label>',
            '        <span v-show="level >= 1 " class="pwd-level u-color1"></span> <span v-show="level >= 2" class="pwd-level u-color2"></span> <span v-show="level >= 3 " class="pwd-level u-color3"></span>',
            '      </div>',
            '      <div class="dialog-item-box">',
            '        <label for="">{{$t(\'user.surePwd\')}}</label>',
            '        <el-input :type=" pwdview.rePwd ? \'text\' : \'password\' " class="dialog-item-input" v-model="dialogData.rePwd" size="mini">',
            '          <i @click=" pwdview.rePwd = !pwdview.rePwd " class="el-icon-view el-input__icon" slot="suffix"></i>',
            '        </el-input>',
            '      </div>',
            '    </div>',
            '    <span slot="footer" class="dialog-footer">',
            '      <el-button size="mini" @click="dialogUser = false">{{$t(\'common.cancel\')}}</el-button>',
            '      <el-button :disabled="legalPwd" size="mini" type="primary" @click="saveUser">{{$t(\'common.save\')}}</el-button>',
            '    </span>',
            '  </el-dialog>',
            '</div>'
        ].join(""),
        data: function () { // 数据
            return {
                pwdview: {
                    pwd: false,
                    rePwd: false
                },
                currentRow: null,
                activeName: 'first',
                dialogUser: false,
                allUsers: {
                    tableData: []
                },
                dialogData: {},
                checkAll: false,
            }
        },
        created: function () {
            //获取用户管理列表
            this.getUsersList();
        },
        filters: {
            onlineFilter: function (status) {
                return status == '0' ? '离线' : status == 1 ? '在线' : '暂无'
            },
            userTypeFilter: function (type) {
                var types = ['管理员', '普通用户', '观众',]
                return types[type];
            }
        },
        computed: {
            legalUser: function () {
                return this.checkUsername(this.dialogData.UserName)
            },
            legalPwd: function () {
                return this.checkPassword(this.dialogData.Password)
            },
            level: function () {
                return this.checkPassWordLevel(this.dialogData.Password);
            },
            deleteDisabled: function () {
                return !this.currentRow || (this.allUsers.tableData.filter(function (item) { return item.UserType == 0 }).length < 2 && this.currentRow.UserType == 0)
            }
        },
        methods: {
            getUsersList: function () {
                var _this = this;
                usersList({}).then(function (res) {
                    var data = res.data;
                    if (data.code == 1000) {
                        _this.allUsers.tableData = data.data
                    } else {
                        _this.$message.error("获取用户管理列表失败")
                    }
                }).catch(function (res) {
                    _this.$message.error("用户管理列表请求失败")
                    return;
                })
            },
            addUser: function () {
                this.dialogData = {
                    UserName: '',
                    UserType: 0,   // 0管理员，1普通用户 2 观众
                    // adminPwd: '',
                    Password: '',
                    rePwd: '',
                    Permission: 0,
                    Online: 0,
                    LoginTime: toDate(new Date().getTime()),
                    CreateTime: toDate(new Date().getTime()),
                }
                this.currentRow = null;
                this.$refs.singleTable.setCurrentRow(null);
                this.dialogUser = true
            },
            updateUser: function () {
                // var p = []
                // for (var k = 0; k < this.currentRow.Permission.length; k++) {
                //     if (this.currentRow.Permission[k] === '1') {
                //         p.push(k + '')
                //     }
                // }
                this.dialogData = this.currentRow;
                // this.dialogData.Permission = p;
                this.dialogUser = true
            },
            delUser: function () {
                var _this = this;
                this.$confirm('确定删除吗?', '提示', {
                    confirmButtonText: '确定',
                    cancelButtonText: '取消',
                    type: 'warning'
                }).then(function () {
                    usersDel(_this.currentRow).then(function (res) {
                        var data = res.data;
                        if (data.code == 1000) {
                            _this.getUsersList()
                        } else {
                            _this.$message.error("删除用户失败")
                        }
                    }).catch(function (res) {
                        _this.$message.error("删除用户请求失败")
                    })
                })
            },
            handleCheckAllChange: function (val) {
                this.dialogData.Permission = val ? this.labels.map(function (res) {
                    return res.value
                }) : []
            },
            handleCheckedChange: function (value) {
                let checkedCount = value.length;
                this.checkAll = checkedCount === this.labels.length;
            },
            handleCurrentChange: function (val, old) {
                this.currentRow = val;
            },
            saveUser: function () {
                var _this = this;
                // var Permission = []
                // for (var i = 0; i < 32; i++) {
                //     Permission.push('0')
                // }
                // for (var j = 0; j < this.dialogData.Permission.length; j++) {
                //     Permission[this.dialogData.Permission[j]] = '1'
                // }
                // if(!_this.checkUsername(_this.dialogData.UserName) || _this.dialogData.UserName.length < 6) {
                //     _this.$message.error('用户名必须5位以上的数字字母组合');
                //     return
                // }
                if (_this.dialogData.Password == _this.dialogData.rePwd) {
                    this.dialogData.rePwd = undefined
                } else {
                    _this.$message.error('两次密码不匹配')
                    return
                }
                _this.dialogData.Permission = _this.dialogData.UserType;
                var ajax = _this.currentRow ? usersUpdate : usersAdd
                ajax(_this.dialogData).then(function (res) {
                    _this.dialogUser = false
                    var data = res.data;
                    if (data.code == 1000) {
                        _this.getUsersList()
                    } else {
                        _this.$message.error("添加用户失败")
                    }
                }).catch(function (res) {
                    _this.$message.error("添加用户请求失败")
                    _this.dialogUser = false
                    return;
                })
            },
            checkUsername: function (username) {
                if(!username) {
                    return true
                }
                if(username.length < 6 || username.length > 18) {
                    return true
                }
                var zg = /^[a-zA-Z]{1}[0-9A-Za-z]*$/;
                if (zg.test(username)) {
                    return false;
                } else {
                    return true;
                }
            },
            checkPassWordLevel: function (password) {
                var securityLevelFlag = 0;
                if(!password) {
                    return 0;
                }
                if (password.length < 6) {
                    return 1;
                } else {
                    if (/[A-Z]/.test(password) || /[a-z]/.test(password)) {   // 有字母  +1 
                        securityLevelFlag++;    //uppercase
                    }
                    if (/[0-9]/.test(password)) {   // 有数字  +1 
                        securityLevelFlag++;    //digital
                    }
                    if (this.containSpecialChar(password)) {  //有符号  +1 
                        securityLevelFlag++;    //specialcase
                    }
                    return securityLevelFlag;
                }
            },
            checkPassword: function (password) {
                if(!password) {
                    return true
                }
                if(password.length < 6 || password.length > 18) {
                    return true
                }
                var hasWord = (/[A-Z]/.test(password) || /[a-z]/.test(password))
                var hasNumber = (/[0-9]/.test(password));
                if(hasWord && hasNumber) {
                    return false
                } else {
                    return true;
                }
            },
            containSpecialChar: function (str) {
                var containSpecial = RegExp(/[(\ )(\~)(\!)(\@)(\#)(\$)(\%)(\^)(\&)(\*)(\()(\))(\-)(\_)(\+)(\=)(\[)(\])(\{)(\})(\|)(\\)(\;)(\:)(\')(\")(\,)(\.)(\/)(\<)(\>)(\?)(\)]+/);
                return (containSpecial.test(str));
            }
        }
    })
})
