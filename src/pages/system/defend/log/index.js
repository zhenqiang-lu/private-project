define(function (require, exports, module) {
    require('./index.css');

    var logList = require('../../../../api/system.js').logList;
    var clearLog = require('../../../../api/system.js').clearLog;
    var exportLog = require('../../../../api/system.js').exportLog;

    module.exports = Vue.component('index', {
        template: ['<div>',
            '  <el-tabs v-model="activeName" @tab-click="tabClick">',
            '    <el-tab-pane :label="$t(\'update.update\')" name="first">UPGRADE</el-tab-pane>',
            '    <el-tab-pane :label="$t(\'log.log\')" name="second">',
            '      <el-row style="height: 28px; display: flex; justify-content: space-between; text-align: left">',
            '        <el-col>',
            '          <label class="user-log-label" for="">{{$t(\'log.mainType\')}}：</label>',
            '          <el-select clearable style="width: 170px;" size="mini" v-model="log.Type">',
            '            <el-option :label="$t(\'log.allType\')" :value="0"></el-option>',
            '          </el-select>',
            '        </el-col>',
            '        <el-col>',
            '          <label class="user-log-label" for="">{{$t(\'common.startTime\')}}：</label>',
            '          <el-date-picker value-format="yyyy-MM-dd HH:mm:ss" style="width: 170px;" v-model="log.BeginTime" size="mini" type="datetime" placeholder="选择日期时间"></el-date-picker>',
            '        </el-col>',
            '        <el-col>',
            '          <label class="user-log-label" for="">{{$t(\'common.endTime\')}}：</label>',
            '          <el-date-picker value-format="yyyy-MM-dd HH:mm:ss" style="width: 170px;" v-model="log.EndTime" size="mini" type="datetime" placeholder="选择日期时间"></el-date-picker></el-col>',
            '        </el-col>',
            '        <span style="display: flex">',
            '          <el-button @click="getLogs" type="success" size="mini">{{$t(\'log.search\')}}</el-button>',
            '          <el-button @click="handleExport" class="right" type="primary" size="mini" >{{$t(\'log.export\')}}</el-button>',
            '          <el-button @click="clearLogs" class="right" type="danger" size="mini" >{{$t(\'log.clear\')}}</el-button>',
            '        </span>',
            '      </el-row>',
            '      <el-table border :data="tableData" style="width: 100%; margin-top: 10px;">',
            '        <el-table-column width="80px" align="center" type="index" :label="$t(\'common.number\')"></el-table-column>',
            '        <el-table-column align="center" prop="User" :label="$t(\'log.localUser\')"></el-table-column>',
            '        <el-table-column align="center" prop="Type" :label="$t(\'log.mainType\')"></el-table-column>',
            '        <el-table-column align="center" prop="Result" :label="$t(\'log.result\')"></el-table-column>',
            '        <el-table-column align="center" prop="OperateTime" :label="$t(\'log.time\')"></el-table-column>',
            // '        <el-table-column align="center" prop="SubTime" :label="$t(\'log.secondType\')"></el-table-column>',
            '        <el-table-column align="center" prop="RemoteAddress" :label="$t(\'log.host\')"></el-table-column>',
            '      </el-table>',
            '      <el-pagination style="margin-top: 10px;" background @current-change="handleCurrentChange" :current-page.sync="pager.currentPage" :page-sizes="[10, 20, 50]" :page-size="pager.pageSize" layout="prev, pager, next, total" :total="pager.total"></el-pagination>',
            '    </el-tab-pane>',
            '  </el-tabs>',
            '</div>'
        ].join(""),
        data: function () { // 数据
            return {
                activeName: 'second',
                log: {
                    Type: 0,
                    // SubType: '',
                    BeginTime: '',
                    EndTime: '',
                   
                },
                tableData: [],
                pager: {
                    currentPage: 1,
                    pageSize: 10,
                    total: 0
                }
            }
        },
        created: function () {
            this.getLogs();
        },
        methods: {
            tabClick: function () {
                if(this.activeName === 'first') {
                    this.$router.push({
                        path: '/admin/systemDefend',
                        query: {
                            num: this.$route.query.num
                        }
                    })
                }else{
                    return
                }
            },
            handleCurrentChange: function (page) {
                this.pager.currentPage = page
                this.getLogs();
            },
            getLogs: function () {
                var _this = this;
                var params = Object.assign({CurrentPage: _this.pager.currentPage, PageSize: _this.pager.pageSize}, _this.log);
                logList(params).then(function (res) {
                    var data = res.data;
                    if (data.code == 1000) {
                        _this.$message.success("获取日志列表成功")
                        _this.log.tableData = data.data.TableData
                        this.pager.total = data.data.TotalItem
                    } else {
                        _this.$message.error("获取日志列表失败")
                    }
                }).catch(function (res) {
                    _this.$message.error("获取日志列表请求失败")
                    return;
                })
            },
            clearLogs: function () {
                clearLog().then(function (res) {
                    _this.$message.success("清除成功")
                }).catch(function (res) {
                    _this.$message.error("清除失败")
                })
            },
            handleExport: function () {
                exportLog().then(function (res) {
                    var data = res.data;
                    window.location.href = data.FilePath
                }).catch(function (res) {
                    _this.$message.error("导出失败")
                })
                
            }
        }
    })
})
