define(function (require, exports, module) {
    require('./index.css');
    var isEmpty = require('../../../../tools/tools.js').isEmpty;
    // 引入请求接口
    var basic = require('../../../../api/system.js').basic;
    var getBasic = require('../../../../api/system.js').getBasic;
    var getNameAndNumber = require('../../../../api/system.js').getNameAndNumber;

    module.exports = Vue.component('index', {
        template: ['<div>',
            '  <el-tabs v-model="activeName" @tab-click="tabClick">',
            '    <el-tab-pane :label="$t(\'basic.basic\')" name="first" >',
            '      <div class="basic-item-box">',
            '        <label for="">{{ $t(\'basic.deviceName\') }}</label>',
            '        <el-input class="msg-input" size="mini" v-model="basicMsg.Name"></el-input>',
            '      </div>',
            '      <div class="basic-item-box">',
            '        <label for="">{{ $t(\'basic.deviceNum\') }}</label>',
            '        <el-input class="msg-input" size="mini" v-model="basicMsg.Num"></el-input>',
            '      </div>',
            '      <div class="basic-item-box">',
            '        <label for="">{{ $t(\'basic.deviceType\') }}</label>',
            '        <el-input class="msg-input" disabled size="mini" v-model="basicMsg.deviceType"></el-input>',
            '      </div>',
            '      <div class="basic-item-box">',
            '        <label for="">{{ $t(\'basic.serialNumber\') }}</label>',
            '        <el-input class="msg-input" disabled size="mini" v-model="basicMsg.serialNumber"></el-input>',
            '      </div>',
            '      <div class="basic-item-box">',
            '        <label for="">{{ $t(\'basic.hardwareVersion\') }}</label>',
            '        <el-input class="msg-input" disabled size="mini" v-model="basicMsg.hardwareVersion"></el-input>',
            '      </div>',
            '      <div class="basic-item-box">',
            '        <label for="">{{ $t(\'basic.softwareVersion\') }}</label>',
            '        <el-input class="msg-input" disabled size="mini" v-model="basicMsg.softwareVersion"></el-input>',
            '      </div>',
            '      <div class="basic-item-box">',
            '        <label for="">{{ $t(\'basic.webVersion\') }}</label>',
            '        <el-input class="msg-input" disabled size="mini" v-model="basicMsg.webVersion"></el-input>',
            '      </div>',
            '      <div style="text-align: center; margin-top: 10px;">',
            '        <el-button @click="saveBasicMsg" icon="el-icon-receiving" size="mini" type="success">{{ $t(\'common.save\') }}</el-button>',
            '      </div>',
            '    </el-tab-pane>',
            '    <el-tab-pane :label="$t(\'time.time\')" name="second">TIMESET</el-tab-pane>',
            '  </el-tabs>',
            '</div>'
        ].join(""),
        data: function () { // 数据
            return {
                activeName: 'first',
                basicMsg: {
                    Name: '',
                    Num: '',
                    deviceType: 'deviceType',
                    serialNumber: '',
                    hardwareVersion: '',
                    softwareVersion: '',
                    webVersion: '',
                }
            }
        },
        created: function () {
            // //获取基本信息
            this.getBasic();
        },
        methods: {
            tabClick: function (tab, event) {
                // console.log(tab,event)
                if (tab.name == 'first') {
                    return
                    //获取基本信息
                    // this.getBasic();
                } else if (tab.name == 'second') {
                    this.$router.push({
                        path: '/admin/timeConfig',
                        query: {
                            num: this.$route.query.num
                        }
                    })
                }
            },
            getBasic: function () {
                var _this = this;
                getNameAndNumber({}).then(function (res) {
                    var data = res.data;
                    console.log(data)
                    if (data.code == 1000) {
                        _this.basicMsg.Name = data.data.Name
                        _this.basicMsg.Num = data.data.Num
                    } else {
                        _this.$message.error("设备名称和设备编号获取失败")
                    }
                }).catch(function (res) {
                    _this.$message.error("设备名称和设备编号请求失败")
                    return;
                })
                getBasic({}).then(function (res) {
                    var data = res.data;
                    console.log(data)
                    if (data.code == 1000) {
                        _this.basicMsg.deviceType = data.data.Type
                        _this.basicMsg.serialNumber = data.data.ID
                        _this.basicMsg.hardwareVersion = data.data.SW
                        _this.basicMsg.softwareVersion = data.data.HW
                        _this.basicMsg.webVersion = data.data.Web
                    } else {
                        _this.$message.error("基本信息获取失败")
                    }
                }).catch(function (res) {
                    _this.$message.error("基本信息请求失败")
                    return;
                })
            },
            saveBasicMsg: function () {
                var _this = this;
                var sendData = {
                    Name: _this.basicMsg.Name,
                    Num: _this.basicMsg.Num
                }
                if (isEmpty(sendData)) {
                    _this.$message.error("请填写设备名称和设备编号")
                    return
                }
                basic(sendData).then(function (res) {
                    var data = res.data;
                    if (data.code == 1000) {
                        _this.$message.success("保存成功")
                    } else {
                        _this.$message.error("保存失败")
                    }
                }).catch(function (res) {
                    _this.$message.error("请求失败")
                    return;
                })

            },
        }
    })
})
