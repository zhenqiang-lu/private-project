define(function (require, exports, module) {
    require('./index.css');
    var toDate = require('../../../../tools/tools.js').toDate;
    // 引入请求接口
    var getTimeConfig = require('../../../../api/system.js').getTimeConfig;
    var timeConfig = require('../../../../api/system.js').timeConfig;

    module.exports = Vue.component('index', {
        template: ['<div>',
            '  <el-tabs v-model="activeName" @tab-click="tabClick">',
            '    <el-tab-pane :label="$t(\'basic.basic\')" name="first">INFO</el-tab-pane>',
            '    <el-tab-pane :label="$t(\'time.time\')" name="second">',
            '      <div class="basic-item-box">',
            '        <label for="">{{ $t(\'time.zone\') }}</label>',
            '        <el-select class="msg-input" size="mini" v-model="timeConfig.Zone">',
            '          <el-option :label="$t(\'time.one\')" :value="0"></el-option>',
            '          <el-option :label="$t(\'time.two\')" :value="1"></el-option>',
            '          <el-option :label="$t(\'time.three\')" :value="2"></el-option>',
            '          <el-option :label="$t(\'time.four\')" :value="3"></el-option>',
            '          <el-option :label="$t(\'time.five\')" :value="4"></el-option>',
            '          <el-option :label="$t(\'time.six\')" :value="5"></el-option>',
            '          <el-option :label="$t(\'time.seven\')" :value="6"></el-option>',
            '          <el-option :label="$t(\'time.eight\')" :value="7"></el-option>',
            '          <el-option :label="$t(\'time.nine\')" :value="8"></el-option>',
            '          <el-option :label="$t(\'time.ten\')" :value="9"></el-option>',
            '          <el-option :label="$t(\'time.eleven\')" :value="10"></el-option>',
            '          <el-option :label="$t(\'time.twelve\')" :value="11"></el-option>',
            '          <el-option :label="$t(\'time.thirteen\')" :value="12"></el-option>',
            '          <el-option :label="$t(\'time.fourteen\')" :value="13"></el-option>',
            '          <el-option :label="$t(\'time.fifteen\')" :value="14"></el-option>',
            '          <el-option :label="$t(\'time.sixteen\')" :value="15"></el-option>',
            '          <el-option :label="$t(\'time.seventeen\')" :value="16"></el-option>',
            '          <el-option :label="$t(\'time.eighteen\')" :value="17"></el-option>',
            '          <el-option :label="$t(\'time.nineteen\')" :value="18"></el-option>',
            '          <el-option :label="$t(\'time.twenty\')" :value="19"></el-option>',
            '          <el-option :label="$t(\'time.twentyOne\')" :value="20"></el-option>',
            '          <el-option :label="$t(\'time.twentyTwo\')" :value="21"></el-option>',
            '          <el-option :label="$t(\'time.twentyThree\')" :value="22"></el-option>',
            '          <el-option :label="$t(\'time.twentyFour\')" :value="23"></el-option>',
            '          <el-option :label="$t(\'time.twentyFive\')" :value="24"></el-option>',
            '          <el-option :label="$t(\'time.twentySix\')" :value="25"></el-option>',
            '          <el-option :label="$t(\'time.twentySeven\')" :value="26"></el-option>',
            '          <el-option :label="$t(\'time.twentyEight\')" :value="27"></el-option>',
            '          <el-option :label="$t(\'time.twentyNine\')" :value="28"></el-option>',
            '          <el-option :label="$t(\'time.thirty\')" :value="29"></el-option>',
            '          <el-option :label="$t(\'time.thirtyOne\')" :value="30"></el-option>',
            '          <el-option :label="$t(\'time.thirtyTwo\')" :value="31"></el-option>',
            '          <el-option :label="$t(\'time.thirtyThree\')" :value="32"></el-option>',
            '          <el-option :label="$t(\'time.thirtyFour\')" :value="33"></el-option>',
            '        </el-select>',
            '      </div>',
            '        <div class="jbs"><el-radio style="width: 100px; text-align: right;" v-model="timeConfig.Type" :label="1">{{ $t(\'time.ntp\') }}</el-radio></div>',
            '      <div>',
            '        <div class="basic-item-box">',
            '          <label for="">{{ $t(\'time.serverAddress\') }}</label>',
            '          <el-input class="msg-input" size="mini" v-model="timeConfig.NTPSAddr"></el-input>',
            '        </div>',
            '        <div class="basic-item-box">',
            '          <label for="">{{ $t(\'time.ntpPort\') }}</label>',
            '          <el-input class="msg-input" size="mini" v-model="timeConfig.NTPSPort"></el-input>',
            '        </div>',
            '        <div class="basic-item-box">',
            '          <label for="">{{ $t(\'time.Period\') }}</label>',
            '          <el-input class="msg-input" size="mini" v-model="timeConfig.Period">',
            '            <span slot="append">{{ $t(\'time.minutes\') }}</span>',
            '          </el-input>',
            '        </div>',
            '        <div class="jbs"><el-radio style="width: 100px; text-align: right;" v-model="timeConfig.Type" :label="0">{{ $t(\'time.hand\') }}</el-radio></div>',
            '      </div>',
            '      <div>',
            '        <div class="basic-item-box">',
            '          <label for="">{{ $t(\'time.setTime\') }}</label>',
            '          <div style="width: 500px; display: inline-block; text-align: left;"><el-date-picker @focus="dateFocus" @change="selectDate" :disabled="timeConfig.Type === 1 " v-model="timeConfig.MulTime" value-format="timestamp" size="mini" type="datetime" :placeholder="$t(\'common.select\')"></el-date-picker>',
            '          <el-checkbox :disabled="timeConfig.Type === 1 " @change="changeSync" v-model="isSync">{{ $t(\'time.synchronization\') }}</el-checkbox></div>',
            '        </div>',
            '      </div>',
            '      <div style="text-align: center; margin-top: 20px;">',
            '        <el-button @click="saveTimeConfig" icon="el-icon-receiving" size="mini" type="success">{{ $t(\'common.save\') }}</el-button>',
            '      </div>',
            '    </el-tab-pane>',
            '  </el-tabs>',
            '</div>'
        ].join(""),
        data: function () { // 数据
            return {
                timer: null,
                activeName: 'second',
                isSync: false,
                timeConfig: {
                    Type: 1,
                    Zone: 0,
                    Period: '',
                    NTPSAddr: '',
                    NTPSPort: '',
                    MulTime: '',
                },
            }
        },

        created: function () {
            this.getTimeConfig();
        },
        methods: {
            dateFocus: function() {
                this.isSync = false;
                this.changeSync()
            },
            selectDate: function() {
                this.goTime()
            },
            changeSync: function () {
                var _this = this
                if (this.isSync) {
                    _this.timeConfig.MulTime = new Date().getTime()
                    this.goTime();
                } else {
                    clearInterval(_this.timer)
                    this.timeConfig.MulTime = ''
                }
            },
            goTime: function() {
                var _this = this
                clearInterval(this.timer)
                this.timer = setInterval(function () {
                    _this.timeConfig.MulTime += 1000
                }, 1000);
            },
            tabClick: function (tab, event) {
                // console.log(tab,event)
                if (tab.name == 'first') {
                    this.$router.push({
                        path: '/admin/systemMsg',
                        query: {
                            num: this.$route.query.num
                        }
                    })
                } else if (tab.name == 'second') {
                    return
                }
            },
            getTimeConfig: function () {
                var _this = this;
                getTimeConfig({}).then(function (res) {
                    var data = res.data;
                    if (data.code == 1000) {
                        _this.timeConfig = data.data
                    } else {
                        _this.$message.error("时间配置获取失败")
                    }
                }).catch(function (res) {
                    _this.$message.error("时间配置请求失败")
                    return;
                })
            },
            saveTimeConfig: function () {
                var _this = this;
                var sendData = Object.assign({}, _this.timeConfig)
                sendData.MulTime = toDate(sendData.MulTime)
                timeConfig(sendData).then(function (res) {
                    var data = res.data;
                    if (data.code == 1000) {
                        _this.$message.success("保存成功")
                    } else {
                        _this.$message.error("保存失败")
                    }
                }).catch(function (res) {
                    _this.$message.error("请求失败")
                    return;
                })
            },
        }
    })
})
