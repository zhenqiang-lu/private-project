define(function(require, exports, module) {
  module.exports = new VueRouter({
    routes: [{
        path: '/',
        component: require('../pages/login/login.js')
      },
      {
        path: '/login',
        component: require('../pages/login/login.js')
      },
      {
        path: '/admin',
        component: require('../components/menu/menu.js'),
        children: [
          {
            path: '/admin/systemMsg',
            name: 'systemMsg',
            component: require('../pages/system/msg/info/index.js')
          },
          {
            path: '/admin/timeConfig',
            name: 'timeConfig',
            component: require('../pages/system/msg/timeset/index.js')
          },
          {
            path: '/admin/systemDefend',
            name: 'systemDefend',
            component: require('../pages/system/defend/upgrade/index.js')
          },
          {
            path: '/admin/systemLog',
            name: 'systemLog',
            component: require('../pages/system/defend/log/index.js')
          },
          {
            path: '/admin/systemUser',
            name: 'systemUser',
            component: require('../pages/system/user/index.js')
          },
          {
            path: '/admin/netConfig',
            name: 'netConfig',
            component: require('../pages/net/basic/tcpip/index.js')
          },
          {
            path: '/admin/netPort',
            name: 'netPort',
            component: require('../pages/net/basic/port/index.js')
          },
          {
            path: '/admin/netMobile',
            name: 'netMobile',
            component: require('../pages/net/basic/mobile/index.js')
          },
          {
            path: '/admin/netWifi',
            name: 'netWifi',
            component: require('../pages/net/basic/wifi/index.js')
          },
          {
            path: '/admin/netHighConfig',
            name: 'netHighConfig',
            component: require('../pages/net/high/platform/index.js')
          },
          {
            path: '/admin/integrate',
            name: 'integrate',
            component: require('../pages/net/high/integrate/index.js')
          },
          {
            path: '/admin/video',
            name: 'video',
            component: require('../pages/media/media/video/index.js')
          },
          {
            path: '/admin/audio',
            name: 'audio',
            component: require('../pages/media/media/audio/index.js')
          },
          {
            path: '/admin/osdConfig',
            name: 'osd',
            component: require('../pages/media/media/osd/index.js')
          },
          {
            path: '/admin/pictureShow',
            name: 'picture',
            component: require('../pages/picture/simple/display/index.js')
          },
          {
            path: '/admin/localDisk',
            name: 'localDisk',
            component: require('../pages/memory/localDisk/index.js')
          },
        ]
      }
    ]
  })
})
